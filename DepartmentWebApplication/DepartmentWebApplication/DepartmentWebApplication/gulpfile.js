let gulp = require('gulp'), // Подключаем Gulp
    sass = require('gulp-sass');


gulp.task('sass', function () {
    return gulp.src('Styles/**/*.sass') // Берем все sass файлы из папки sass и дочерних, если таковые будут
        .pipe(sass())
        .pipe(gulp.dest('./Styles'))
});


gulp.task('watch', function () {
    gulp.watch('Styles/**/*.sass', gulp.parallel('sass'));

});

