﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DepartmentWebApplication.Controllers
{
    public class ExceptionController : Controller
    {
        // GET: Exception
        public ActionResult Index(string message)
        {
            ViewBag.Message = message;
            return View();
        }
    }
}