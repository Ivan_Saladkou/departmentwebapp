﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataAccessLayer;
using DataAccessLayer.Entities;

namespace DepartmentWebApplication.Controllers
{
    public class CoefficientsController : Controller
    {
        private DepartmentEntities db = new DepartmentEntities();

        // GET: Coefficients
        public ActionResult Index()
        {
            return View(db.Coefficients.ToList());
        }

        // GET: Coefficients/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Coefficient coefficient = db.Coefficients.Find(id);
            if (coefficient == null)
            {
                return HttpNotFound();
            }
            return View(coefficient);
        }

        // POST: Coefficients/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в разделе https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Value")] Coefficient coefficient)
        {
            if (ModelState.IsValid)
            {
                var oldCoefficient = db.Coefficients.Find(coefficient.Id);
                oldCoefficient.Value = coefficient.Value;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(coefficient);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
