﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExcelReporting.ExcelGenerators;
using DataAccessLayer.Repositories;
using DataAccessLayer.Enums;
using System.IO;
using DepartmentWebApplication.Models;
using ExcelReporting.ExcelReaders;
using System.Text;
using DepartmentWebApplication.Extensions;
using ExcelReporting.Enums;
using ExcelReporting.Models;

namespace DepartmentWebApplication.Controllers
{
    [RoutePrefix("reports")]
    public class ReportsController : Controller
    {
        private SettingRepository settingRepository = new SettingRepository();

        // GET: Reports
        public ActionResult Index()
        {
            return View();
        }

        [Route("individualPlans")]
        public ActionResult IndividualPlans()
        {
            string plansPath = settingRepository.GetValue(SettingEnum.IndividualPlansPath);
            string reportSavePath = settingRepository.GetValue(SettingEnum.ReportIndividualPlansPath);
            string errorMessage = "";
            IndividualPlansModel plansModel = new IndividualPlansModel();


            if (string.IsNullOrWhiteSpace(reportSavePath))
            {
                errorMessage += SettingEnum.ReportIndividualPlansPath.GetErrorMessageEmpty();
            }
            else
            {
                plansModel.ReportExist = System.IO.File.Exists(reportSavePath + "/report.xlsx");
            }

            if (string.IsNullOrWhiteSpace(plansPath))
            {
                errorMessage += SettingEnum.IndividualPlansPath.GetErrorMessageEmpty();
            }
            else
            {
                plansModel.IndividualPlanPaths = Directory.GetFiles(plansPath, "*.xlsx").Select(e => Path.GetFileName(e)).ToArray();
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                plansModel.ErrorMessage = errorMessage;
            }

            return View("IndividualPlans", plansModel);
        }

        [HttpGet]
        [Route("individualPlans/createreport")]
        public ActionResult IndividualPlansCreateReport()
        {
            string reportSavePath = settingRepository.GetValue(SettingEnum.ReportIndividualPlansPath);
            string plansPath = settingRepository.GetValue(SettingEnum.IndividualPlansPath);
            if (string.IsNullOrWhiteSpace(reportSavePath) || string.IsNullOrWhiteSpace(plansPath))
            {
                return IndividualPlans();
            }

            ReportIndividualPlansGenerator report = new ReportIndividualPlansGenerator();
            report.Generate(plansPath, reportSavePath, "report");
            return Redirect("/reports/individualPlans");
        }

        [HttpGet]
        [Route("individualPlans/create")]
        public ActionResult IndividualPlansCreate()
        {
            string plansPath = settingRepository.GetValue(SettingEnum.IndividualPlansPath);
            string planTempPath = settingRepository.GetValue(SettingEnum.IndividualPlanTempPath);
            string distributionRBPath = settingRepository.GetValue(SettingEnum.DistributionsRBPath);
            StringBuilder errorMessage = new StringBuilder(20);

            if (string.IsNullOrWhiteSpace(plansPath))
            {
                errorMessage.Append(SettingEnum.IndividualPlansPath.GetErrorMessageEmpty());
            }
            if (string.IsNullOrWhiteSpace(planTempPath))
            {
                errorMessage.Append(SettingEnum.IndividualPlanTempPath.GetErrorMessageEmpty());
            }
            if (string.IsNullOrWhiteSpace(distributionRBPath))
            {
                errorMessage.Append(SettingEnum.DistributionsRBPath.GetErrorMessageEmpty());
            }

            if (!string.IsNullOrEmpty(errorMessage.ToString()))
            {
                return View("IndividualPlans", new IndividualPlansModel { ErrorMessage = errorMessage.ToString() });
            }

            IndividualPlanGenerator planGenerator = new IndividualPlanGenerator(plansPath, planTempPath);
            DistributionReader distributionReader = new DistributionReader();
            planGenerator.Generate(distributionReader.Get(distributionRBPath, "Распределение РБ"));
            return Redirect("/reports/individualPlans");
        }



        [HttpGet]
        [Route("individualPlans/downloadreport")]
        public ActionResult IndividualPlansDownloadReport()
        {
            string reportSavePath = settingRepository.GetValue(SettingEnum.ReportIndividualPlansPath);
            return File(System.IO.File.ReadAllBytes(reportSavePath + "\\report.xlsx"), System.Net.Mime.MediaTypeNames.Application.Octet, "report.xlsx");
        }

        [HttpGet]
        [Route("individualPlans/download")]
        public ActionResult IndividualPlanDownload(string employee)
        {
            string plansPath = settingRepository.GetValue(SettingEnum.IndividualPlansPath);
            string fileName = employee;
            string filePath = $"{plansPath}\\{fileName}";
            return File(System.IO.File.ReadAllBytes(filePath), System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [HttpPost]
        [Route("individualPlans/updateplan")]
        public ActionResult IndividualPlanUpdate(HttpPostedFileBase individualplan)
        {
            string fileName = individualplan?.FileName;

            if (fileName == null || fileName.Contains('\\') || fileName.Split('.').Last() != "xlsx")
            {
                return View("IndividualPlans", new IndividualPlansModel { ErrorMessage = "Некорректный файл" });
            }

            string plansPath = settingRepository.GetValue(SettingEnum.IndividualPlansPath);
            if (string.IsNullOrWhiteSpace(plansPath))
            {
                return IndividualPlans();
            }

            string savePath = $@"{plansPath}\{fileName}";
            if (!System.IO.File.Exists(savePath))
            {
                return View("IndividualPlans", new IndividualPlansModel { ErrorMessage = "Файла в каталоге не существует" });
            }
            individualplan.SaveAs($@"{plansPath}\{fileName}");
            return Redirect("/reports/individualPlans");
        }

        [HttpGet]
        [Route("disciplinesinformation")]
        public ActionResult DisciplinesInformation()
        {
            string informationsPath = settingRepository.GetValue(SettingEnum.DisciplinesInformationsPath);
            string errorMessage = "";
            DisciplinesInformationModel informationModel = new DisciplinesInformationModel();

            if (string.IsNullOrWhiteSpace(informationsPath))
            {
                errorMessage += SettingEnum.DisciplinesInformationsPath.GetErrorMessageEmpty();
            }
            else
            {
                informationModel.DisciplinesInformationPaths = Directory.GetFiles(informationsPath, "*.xlsx").Select(e => Path.GetFileName(e)).ToArray();
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                informationModel.ErrorMessage = errorMessage;
            }

            return View(informationModel);
        }

        [HttpGet]
        [Route("disciplinesinformation/create")]
        public ActionResult DisciplinesInformationCreate(string semester)
        {
            Semester semesterEnum = (Semester)Convert.ToInt32(semester);
            string informationsPath = settingRepository.GetValue(SettingEnum.DisciplinesInformationsPath);
            string informationTempPath = settingRepository.GetValue(SettingEnum.DisciplinesInformationTempPath);

            string distributionRBPath = settingRepository.GetValue(SettingEnum.DistributionsRBPath);
            string distributionRFPath = settingRepository.GetValue(SettingEnum.DistributionsRFPath);

            string disciplinesRBPath = settingRepository.GetValue(SettingEnum.DisciplinesBySemesterRBPath);
            string disciplinesRFPath = settingRepository.GetValue(SettingEnum.DisciplinesBySemesterRFPath);

            StringBuilder errorMessage = new StringBuilder(20);

            if (string.IsNullOrWhiteSpace(informationsPath))
            {
                errorMessage.Append(SettingEnum.DisciplinesInformationsPath.GetErrorMessageEmpty());
            }
            if (string.IsNullOrWhiteSpace(informationTempPath))
            {
                errorMessage.Append(SettingEnum.DisciplinesInformationTempPath.GetErrorMessageEmpty());
            }
            if (string.IsNullOrWhiteSpace(distributionRBPath))
            {
                errorMessage.Append(SettingEnum.DistributionsRBPath.GetErrorMessageEmpty());
            }
            if (string.IsNullOrWhiteSpace(distributionRFPath))
            {
                errorMessage.Append(SettingEnum.DistributionsRFPath.GetErrorMessageEmpty());
            }
            if (string.IsNullOrWhiteSpace(disciplinesRBPath))
            {
                errorMessage.Append(SettingEnum.DisciplinesBySemesterRBPath.GetErrorMessageEmpty());
            }
            if (string.IsNullOrWhiteSpace(disciplinesRFPath))
            {
                errorMessage.Append(SettingEnum.DisciplinesBySemesterRFPath.GetErrorMessageEmpty());
            }

            if (!string.IsNullOrEmpty(errorMessage.ToString()))
            {
                return View("DisciplinesInformation", new DisciplinesInformationModel { ErrorMessage = errorMessage.ToString() });
            }

            DisciplinesReader disciplinesReader = new DisciplinesReader();
            DistributionReader distributionReader = new DistributionReader();

            var prb = disciplinesReader.Get(disciplinesRBPath, "ПоСеместрам РБ");
            var prf = disciplinesReader.Get(disciplinesRFPath, "ПоСеместрам РФ");

            var employeesrf = distributionReader.Get(distributionRFPath, "Распределение РФ");//25
            var employeesrb = distributionReader.Get(distributionRBPath, "Распределение РБ");//34

            Func<DisciplineIndividual, bool> disciplinePredicate = d =>
                d.Department == ExcelReporting.Enums.Department.Daytime
                && d.Semester == semesterEnum
                && d.Sum != 0;
            var sortedDisciplinesRb = prb.Where(disciplinePredicate);
            var sortedDisciplinesRf = prf.Where(disciplinePredicate);
            DisciplinesReader.FillWithPeoples(sortedDisciplinesRf, employeesrf);
            DisciplinesReader.FillWithPeoples(sortedDisciplinesRb, employeesrb);

            DisciplinesInformationGenerator informationGenerator = new DisciplinesInformationGenerator(informationsPath, informationTempPath, semesterEnum.GetDisplayName());
            informationGenerator.Generate(sortedDisciplinesRb.GroupBy(e => e.Stage).Concat(sortedDisciplinesRf.GroupBy(e => e.Stage)));

            return Redirect("/reports/disciplinesinformation");
        }

        [HttpGet]
        [Route("disciplinesinformation/download")]
        public ActionResult DisciplinesInformationDownload(string name)
        {
            string informationPath = settingRepository.GetValue(SettingEnum.DisciplinesInformationsPath);
            string fileName = name;
            string filePath = $"{informationPath}\\{fileName}";
            return File(System.IO.File.ReadAllBytes(filePath), System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
    }
}