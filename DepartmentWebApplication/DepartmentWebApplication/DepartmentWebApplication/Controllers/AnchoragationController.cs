﻿using DataAccessLayer;
using DataAccessLayer.Entities;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Data.Entity;
using System.Collections.Generic;

namespace DepartmentWebApplication.Controllers
{
    public class AnchoragationController : Controller
    {
        private DepartmentEntities db = new DepartmentEntities();
        // GET: Anchoragation
        public ActionResult Index()
        {
            //выбрать планы, у которых у дисциплин .DepartmentId != null

            List<Plan> plans = db.Plans
                 .Include(p => p.Disciplines
                         .Select(d => d.Subject))
                 .Include(p => p.Speciality)
                 .ToList();


            foreach (Plan plan in plans)
            {
                var list = plan.Disciplines.ToList();
                list.RemoveAll(dis => dis.DepartmentId != null);
                plan.Disciplines = list;
            }

            ViewBag.Plans = plans;

            ViewBag.Faculties = db.Faculties
                .Include(f => f.Departments
                .Select(dep => dep.Disciplines));

            return View();
        }


        // POSt: Anchoragation
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Anchorage(string[] departments, string[] disciplines)
        {
            SetDepartmentIdToDistributedDisciplines(departments);
            SetUnanchoragedDisciplines(disciplines);
            return RedirectToAction("Index", "Disciplines");
        }

        private void SetDepartmentIdToDistributedDisciplines(string[] departments)
        {
            Department department = null;
            for (int i = 0; i < departments.Length; i++)
            {
                int id = Convert.ToInt32(departments[i].Split('/')[0]);
                using (var database = new DepartmentEntities())
                {
                    if (id < 0)
                    {
                        department = database.Departments
                            .Include(d => d.Disciplines)
                            .FirstOrDefault(d => d.Id == -id);
                    }
                    else
                    {
                        if (department != null)
                        {
                            Discipline discipline = database.Disciplines.Find(id);
                            discipline.DepartmentId = department.Id;
                            database.SaveChanges();
                        }
                    }
                }
            }
        }

        private void SetUnanchoragedDisciplines(string[] disciplines)
        {
            if (disciplines == null)
            {
                return;
            }
            for (int i = 0; i < disciplines.Length; i++)
            {
                int id = Convert.ToInt32(disciplines[i].Split('/')[0]);
                using (var database = new DepartmentEntities())
                {
                    Discipline discipline = database.Disciplines.Find(id);
                    discipline.DepartmentId = null;
                    database.SaveChanges();
                }
            }
        }
    }




}
