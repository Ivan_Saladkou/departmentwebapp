﻿using DataAccessLayer.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DepartmentWebApplication.Controllers
{
    public class ContingentController : Controller
    {
        DataBaseDepartment db = new DataBaseDepartment();
        // GET: Contingent
        public ActionResult Index()
        {
            List<Speciality> specialities = db.Specialities.GetAll();
            List<Contingent> contingents = new List<Contingent>();

            Contingent contingentAdd;
            foreach(Speciality speciality in specialities)
            {
                if (speciality.Contingents.Count == 0)
                {
                    contingentAdd = new Contingent();
                    contingentAdd.Speciality = speciality;
                    contingentAdd.SpecialityId = speciality.Id;
                    contingentAdd.Courses = new List<Course>();
                    contingents.Add(contingentAdd);
                }
                else
                {
                    contingents.AddRange(speciality.Contingents);
                }
            }
            foreach (Contingent contingent in contingents)
            {
                for(int i = 1; i <= 5; i++)
                {
                    if (!contingent.Courses.Any(e=> e.Number == i))
                    {
                        contingent.Courses.Add(new Course
                        {
                            Number = i,
                            ContingentId = contingent.Id
                        });
                    }
                }

                contingent.Courses = contingent.Courses.OrderBy(e => e.Number).ToList();
            }
            return View(contingents);
        }

        [HttpPost]
        public void Index(string s)
        {
            string jsonString;
            List<Contingent> contingents;
            using (var reader = new StreamReader(HttpContext.Request.InputStream))
            {
                jsonString = reader.ReadToEnd();
            }
            contingents = JsonConvert.DeserializeObject<List<Contingent>>(jsonString);

            List<Contingent> contingentsDB = db.Contingents.GetAll();

            List<Contingent> contingentsUpdate = contingents.Where(e => e.Id != 0).ToList(); //список контингента для обновления
            List<Contingent> contingentsAdd = contingents.Where(e => e.Id == 0).ToList(); //для добавления

            List<Course> courses = db.Courses.GetAll();

            foreach (Contingent contingent in contingentsAdd)
            {
                contingent.Date = DateTime.Now;
                db.Contingents.Add(contingent);
            }

            foreach (Contingent contingent in contingentsUpdate)
            {
                foreach (Course course in contingent.Courses)
                {
                    Course courseOld = courses.First(e => e.ContingentId == course.ContingentId && e.Number == course.Number);
                    db.Courses.Edit(courseOld.Id, course);
                }
            }
        }

        // GET: Contingents/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Contingents/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Contingents/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Contingents/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Contingents/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Contingents/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Contingents/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
