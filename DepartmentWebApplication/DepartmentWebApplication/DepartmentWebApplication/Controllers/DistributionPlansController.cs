﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using DataAccessLayer;
using DataAccessLayer.Entities.Load;
using DataAccessLayer.Enums;


namespace DepartmentWebApplication.Controllers
{
    public class DistributionPlansController : Controller
    {
        private DepartmentEntities db = new DepartmentEntities();

        // GET: DistributionPlans
        public ActionResult Index()
        {
            return View(new DistributionPlan());
        }

        [HttpPost]
        public PartialViewResult DistributionFilter(int budget, int stage, int form, int time)
        {
            var distributionPlans = db.DistributionPlans.Where(d => d.Budget == (Budget)budget && d.Stage == (Stage)stage && d.FormStudy == (FormStudy)form && d.TimeOfYear == (TimeOfYear)time).ToList();
            var teachers = db.Teachers.ToList();
            ViewBag.Teachers = teachers;
            ViewBag.DistributionPlans = distributionPlans;
            return PartialView();
        }


        [HttpPost]
        public PartialViewResult SearchInfo(int teacherId, int distributionId)
        {
            var distribution = db.DistributionPlans.Find(distributionId);
            DistributionPlan distributionPlan = new DistributionPlan();
            Load load = new Load();
            if (Convert.ToBoolean(distributionId) && Convert.ToBoolean(teacherId))
            {
                var loads = db.Loads.Where(l => l.DistributionPlan.Id == distributionId).Where(l => l.Teacher.Id == teacherId);
                if (loads.Count() > 0)
                {
                    load = loads.First();
                }
                else if (loads.Count() == 0)
                {
                    load = new Load
                    {
                        TeacherId = teacherId,
                        DistributionPlanId = distributionId
                    };
                    db.Loads.Add(load);
                    db.SaveChanges();
                }
            }
            return PartialView(Tuple.Create(distribution, load));
        }


        [HttpPost]
        public PartialViewResult ChangeValue(int pointNum, bool isIncrease, int teacherId, int distributionId, bool isAll)
        {
            teacherId = Convert.ToInt32(teacherId);
            distributionId = Convert.ToInt32(distributionId);
            var load = db.Loads.Where(l => l.DistributionPlan.Id == distributionId).Where(l => l.Teacher.Id == teacherId).First();
            var distribution = db.DistributionPlans.Find(distributionId);
            var staticDistribution = db.StaticDistributionPlans.Where(l => l.DistributionPlanId == distributionId).First();
            var discretesDict = staticDistribution.GetDiscretesDict();
            double value = discretesDict[Convert.ToString((DistributionWorkType)pointNum)];
            var distributionValue = distribution.GetType().GetProperty(Convert.ToString((DistributionWorkType)pointNum)).GetValue(distribution);
            var loadValue = load.GetType().GetProperty(Convert.ToString((DistributionWorkType)pointNum)).GetValue(load);
            if (isIncrease)
            {
                if (isAll)
                {
                    load.GetType().GetProperty(Convert.ToString((DistributionWorkType)pointNum)).SetValue(load, (double)loadValue + (double)distributionValue);
                    distribution.GetType().GetProperty(Convert.ToString((DistributionWorkType)pointNum)).SetValue(distribution, 0);
                }
                else
                {
                    if (((double)distributionValue - value) > 0)
                    {
                        load.GetType().GetProperty(Convert.ToString((DistributionWorkType)pointNum)).SetValue(load, Math.Round((double)loadValue + value, 2));
                        distribution.GetType().GetProperty(Convert.ToString((DistributionWorkType)pointNum)).SetValue(distribution, Math.Round((double)distributionValue - value, 2));
                    }
                    else
                    {
                        load.GetType().GetProperty(Convert.ToString((DistributionWorkType)pointNum)).SetValue(load, (double)loadValue + (double)distributionValue);
                        distribution.GetType().GetProperty(Convert.ToString((DistributionWorkType)pointNum)).SetValue(distribution, 0);
                    }
                }
                
            }
            else
            {
                if (isAll)
                {
                    load.GetType().GetProperty(Convert.ToString((DistributionWorkType)pointNum)).SetValue(load, 0);
                    distribution.GetType().GetProperty(Convert.ToString((DistributionWorkType)pointNum)).SetValue(distribution, (double)loadValue + (double)distributionValue);
                }
                else
                {
                    if (((double)loadValue - value) > 0)
                    {
                        load.GetType().GetProperty(Convert.ToString((DistributionWorkType)pointNum)).SetValue(load, Math.Round((double)loadValue - value, 2));
                        distribution.GetType().GetProperty(Convert.ToString((DistributionWorkType)pointNum)).SetValue(distribution, Math.Round((double)distributionValue + value, 2));
                    }
                    else
                    {
                        load.GetType().GetProperty(Convert.ToString((DistributionWorkType)pointNum)).SetValue(load, 0);
                        distribution.GetType().GetProperty(Convert.ToString((DistributionWorkType)pointNum)).SetValue(distribution, (double)loadValue + (double)distributionValue);
                    }
                }
            }
            db.SaveChanges();

            return PartialView("SearchInfo", Tuple.Create(distribution, load));
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Index(int distributionId, int teaherId)
        //{
        //    DistributionPlan distributionPlan = db.DistributionPlans.Find(distributionId);
        //    ViewBag.DistributionPlans = db.DistributionPlans.Include(d => d.Discipline).Include(d => d.Discipline.Subject).Include(d => d.Discipline.Plan.Speciality).ToList();
        //    ViewBag.Teachers = db.Teachers.ToList();
        //    var loads = db.Loads.Where(l => l.DistributionPlan.Id == distributionId).Where(l => l.Teacher.Id == teaherId);
        //    if (loads.Count() == 1)
        //    {
        //        ViewBag.Load = loads.First();
        //    }
        //    else if (loads.Count() == 0)
        //    {
        //        db.Loads.Add(new Load
        //        {
        //            TeacherId = teaherId,
        //            DistributionPlanId = distributionId
        //        });
        //    }

        //    return View(distributionPlan);
        //}

        // GET: DistributionPlans/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DistributionPlan distributionPlan = db.DistributionPlans.Find(id);
            if (distributionPlan == null)
            {
                return HttpNotFound();
            }
            return View(distributionPlan);
        }

        // GET: DistributionPlans/Create
        public ActionResult Create()
        {
            ViewBag.DisciplineId = new SelectList(db.Disciplines, "Id", "Id");
            return View();
        }

        // POST: DistributionPlans/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в разделе https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DisciplineId,Budget,FormStudy,Stage,TimeOfYear,Course,BudgetStudents,OffBudgetStudents,BudgetGroups,OffBudgetGroups,Lectures,Labs,Practice,ModuleRatingSystem,IndividualsTasks,CourseProject,Consultations,ReviewingTests,Offsets,Exams,EducationPractice,Internship,ThesisProject,StateExaminationComission")] DistributionPlan distributionPlan)
        {
            if (ModelState.IsValid)
            {
                db.DistributionPlans.Add(distributionPlan);
                db.StaticDistributionPlans.Add(new StaticDistributionPlan
                {
                    DistributionPlanId = distributionPlan.Id,
                    BudgetStudents = distributionPlan.BudgetStudents,
                    OffBudgetStudents = distributionPlan.OffBudgetStudents,
                    BudgetGroups = distributionPlan.BudgetGroups,
                    OffBudgetGroups = distributionPlan.OffBudgetGroups,
                    Lectures = distributionPlan.Lectures,
                    Labs = distributionPlan.Labs,
                    Practice = distributionPlan.Practice,
                    ModuleRatingSystem = distributionPlan.ModuleRatingSystem,
                    IndividualsTasks = distributionPlan.IndividualsTasks,
                    CourseProject = distributionPlan.CourseProject,
                    Consultations = distributionPlan.Consultations,
                    ReviewingTests = distributionPlan.ReviewingTests,
                    Offsets = distributionPlan.Offsets,
                    Exams = distributionPlan.Exams,
                    ThesisProject = distributionPlan.ThesisProject,
                    Internship = distributionPlan.Internship,
                    StateExaminationComission = distributionPlan.StateExaminationComission
                });
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DisciplineId = new SelectList(db.Disciplines, "Id", "Id", distributionPlan.DisciplineId);
            return View(distributionPlan);
        }

        // GET: DistributionPlans/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DistributionPlan distributionPlan = db.DistributionPlans.Find(id);
            if (distributionPlan == null)
            {
                return HttpNotFound();
            }
            ViewBag.DisciplineId = new SelectList(db.Disciplines, "Id", "Id", distributionPlan.DisciplineId);
            return View(distributionPlan);
        }

        // POST: DistributionPlans/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в разделе https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DisciplineId,Budget,FormStudy,Stage,TimeOfYear,Course,BudgetStudents,OffBudgetStudents,BudgetGroups,OffBudgetGroups,Lectures,Labs,Practice,ModuleRatingSystem,IndividualsTasks,CourseProject,Consultations,ReviewingTests,Offsets,Exams,EducationPractice,Internship,ThesisProject,StateExaminationComission")] DistributionPlan distributionPlan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(distributionPlan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DisciplineId = new SelectList(db.Disciplines, "Id", "Id", distributionPlan.DisciplineId);
            return View(distributionPlan);
        }

        // GET: DistributionPlans/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DistributionPlan distributionPlan = db.DistributionPlans.Find(id);
            if (distributionPlan == null)
            {
                return HttpNotFound();
            }
            return View(distributionPlan);
        }

        // POST: DistributionPlans/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DistributionPlan distributionPlan = db.DistributionPlans.Find(id);
            db.DistributionPlans.Remove(distributionPlan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
