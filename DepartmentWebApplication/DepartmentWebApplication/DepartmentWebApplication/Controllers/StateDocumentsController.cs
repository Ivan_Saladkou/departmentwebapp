﻿using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using DataAccessLayer;
using DataAccessLayer.Entities;
using DataAccessLayer.Service;
using ExcelReporting;

namespace DepartmentWebApplication.Controllers
{
    public class StateDocumentsController : Controller
    {
        private DepartmentEntities db = new DepartmentEntities();
        private StateService stateService = new StateService();
        private StateExcelGenerator excelGenerator = new StateExcelGenerator();

        // GET: StateDocuments
        public ActionResult Index()
        {
            var stateDocumentes = db.StateDocumentes.Include(s => s.Department);
            return View(stateDocumentes.ToList());
        }

        // GET: StateDocuments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StateDocument stateDocument = db.StateDocumentes
                .Include(s => s.Department)
                .Include(s => s.StateBlocks
                    .Select(b => b.StateRows))
                .First(s => s.Id == id);
            if (stateDocument == null)
            {
                return HttpNotFound();
            }
            ViewBag.State = stateDocument;

            return View(stateDocument);
        }

        public ActionResult Import(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StateDocument stateDocument = db.StateDocumentes
                .Include(s => s.Department)
                .Include(s => s.StateBlocks
                    .Select(b => b.StateRows))
                .First(s => s.Id == id);
            if (stateDocument == null)
            {
                return HttpNotFound();
            }
            try
            {
                excelGenerator.Generate(stateDocument.Id);
            }
            catch (FileNotFoundException exception)
            {
                return RedirectToAction("Index", "Exception", new { message = exception.Message });
            }

            ViewBag.State = stateDocument;
            return RedirectToAction("Details", new { id = stateDocument.Id });
        }


        // GET: StateDocuments/Create
        public ActionResult Create()
        {
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Name");
            return View(new StateDocument());
        }

        // POST: StateDocuments/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в разделе https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Date,Budget,DepartmentId,FormationDate")] StateDocument stateDocument)
        {
           
            if (ModelState.IsValid)
            {

                db.StateDocumentes.Add(stateDocument);
                db.SaveChanges();

                StateDocument state = db.StateDocumentes.Include(s => s.Department)
                                                 .Include(s => s.StateBlocks
                                                    .Select(b => b.StateRows))
                                                 .First(s => s.FormationDate == stateDocument.FormationDate);
                stateService.FillStateDocument(stateDocument);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Name", stateDocument.DepartmentId);
            return View(stateDocument);
        }

        // GET: StateDocuments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StateDocument stateDocument = db.StateDocumentes.Find(id);
            if (stateDocument == null)
            {
                return HttpNotFound();
            }
            return View(stateDocument);
        }


        // POST: StateDocuments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            StateDocument stateDocument = db.StateDocumentes.Find(id);
            db.StateDocumentes.Remove(stateDocument);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
