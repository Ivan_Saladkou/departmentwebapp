﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataAccessLayer;
using DataAccessLayer.Entities;

namespace DepartmentWebApplication.Controllers
{
    //теперь назывется Disciplines
    public class DisciplinesController : Controller
    {
        private DepartmentEntities db = new DepartmentEntities();

        private List<string> DiplomaDisciplines = new List<string> {
            "Преддипломная практика",
            "Председатель ГЭК",
            "Члены ГЭК",
            "Нормаконтроль",
            "Утверждение",
            "Рецензирование"
        };

        // GET: DisciplineSemesters
        public ActionResult Index()
        {
            var disciplineSemesters = db.Disciplines
                .Include(d => d.Subject)
                .Include(d => d.Department)
                .Include(d => d.Plan.Speciality);

            return View(disciplineSemesters.ToList());
        }

        // GET: DisciplineSemesters/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Discipline disciplineSemester = db.Disciplines.Find(id);
            if (disciplineSemester == null)
            {
                return HttpNotFound();
            }
            return View(disciplineSemester);
        }

        // GET: DisciplineSemesters/Create
        public ActionResult Create(int id)
        {
          
        ViewBag.PlanId = id;
            ViewBag.DisciplineName = new SelectList(db.Subjects.Where(d=>!DiplomaDisciplines.Contains(d.Name)), "Id", "Name");
            return View();
        }

        // POST: DisciplineSemesters/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в разделе https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PlanId,SubjectId,IsWithCommission,MemberAmount," +
            "SemesterNumber,Control,CourseType,Lectures," +
            "Labs,Practices,Seminars,Independents,Ksr")] Discipline discipline)
        {
            if (ModelState.IsValid)
            {
            
                db.Disciplines.Add(discipline);
                if (discipline.SubjectId  == 1 && discipline.IsWithCommission) {
                    for (int i = 2; i <= 6; i++) {
                        db.Disciplines.Add(new Discipline { SubjectId = i, PlanId = discipline.PlanId, SemesterNumber = discipline.SemesterNumber });
                    }
                }
                db.SaveChanges();
                return RedirectToAction("Details", "Plan", new { id = discipline.PlanId });
            }
            //прокинуть на страницу ошибки           
            return RedirectToAction("Index", "Exception",new {message= "Неверно заполнена форма" });

        }

        // GET: DisciplineSemesters/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Discipline discipline = db.Disciplines.Find(id);
            if (discipline == null)
            {
                return HttpNotFound();
            }
            ViewBag.PlanId = id;
            ViewBag.DisciplineName = new SelectList(db.Subjects.Where(d => !DiplomaDisciplines.Contains(d.Name)), "Id", "Name");
            return View();
        }

        // POST: DisciplineSemesters/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в разделе https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,SubjectId,Number,Control,CourseType,Lectures,Labs,Practices,Seminars,Independents,Ksr")] Discipline discipline)
        {
            if (ModelState.IsValid)
            {
                db.Entry(discipline).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details", "Plan", new { id = discipline.PlanId });
            }

            return RedirectToAction("Index", "Exception", new { message = "Неверно заполнена форма" });
           
        }

        // GET: DisciplineSemesters/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Discipline disciplineSemester = db.Disciplines.Find(id);
            if (disciplineSemester == null)
            {
                return HttpNotFound();
            }
            return View(disciplineSemester);
        }

        // POST: DisciplineSemesters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Discipline disciplineSemester = db.Disciplines.Find(id);
            db.Disciplines.Remove(disciplineSemester);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
