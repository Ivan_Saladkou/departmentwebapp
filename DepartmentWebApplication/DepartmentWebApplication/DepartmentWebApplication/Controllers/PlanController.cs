﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DepartmentWebApplication;
using DataAccessLayer.Entities;
using DepartmentWebApplication.Models;
using System.Reflection;
using DataAccessLayer;

namespace DepartmentWebApplication.Controllers
{
    public class PlanController : Controller
    {
        DataBaseDepartment db = new DataBaseDepartment();
        // GET: Plan
        public ActionResult Index()
        {
            return View(db.Plans.GetAll());
        }

        // GET: Plan/details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Plan plan = db.Plans.GetItem((int)id);
            if (plan == null)
            {
                return HttpNotFound();
            }
            return View(plan);
        }

        // POST: Plan/details/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Details(int id, params int[] semesters)
        {
            using (var db = new DepartmentEntities())
            {
                List<Semester> list = db.Semesters.Where(s => s.PlanId == id).ToList();
                db.Semesters.RemoveRange(list);

                for (int i = 0; i < semesters.Length; i++)
                {
                    db.Semesters.Add(new Semester()
                    {
                        Number = i + 1,
                        Duration = semesters[i],
                        PlanId = id
                    });
                }
                db.SaveChanges();
            }
            return Redirect("/Plan/Details/" + id);
        }


        // GET: Plan/Create
        public ActionResult Create()
        {
            Plan plan = new Plan();
            plan.Date = DateTime.Now;
            return View(plan);
        }



        //// POST: Plan/Create
        //// Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        //// сведения см. в разделе https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //// [ValidateAntiForgeryToken]
        //public ActionResult Create(Models.Plan plan)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Plans.Add(mapper.mapFromModel(plan));
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(plan);
        //}


        // POST: Plan/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в разделе https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        // [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Speciality,Speciality.Name,Speciality.NameAbbreviated,Specialization,Cipher,Duration,Date,RegNumber,Stage,FormStudy,Budget")] Plan plan)
        {
            if (ModelState.IsValid)
            {
                int specialityId = db.Specialities.GetAll().Find(e => e.Cipher == plan.Speciality.Cipher)?.Id ?? -1;
                if (specialityId == -1)
                {
                    specialityId = db.Specialities.AddGetId(plan.Speciality);
                }

                plan.Speciality = null; // убирает второе добавление специальности в бд
                plan.SpecialityId = specialityId;
                db.Plans.Add(plan);
                return RedirectToAction("Index");
            }

            return View(plan);
        }

        // GET: Plan/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Plan plan = db.Plans.GetItem((int)id);
            if (plan == null)
            {
                return HttpNotFound();
            }
            ViewData["SpecialityId"] = new SelectList(db.Specialities.GetAll(), "Id", "Name", plan.SpecialityId);
            return View(plan);
        }

        // POST: Plan/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в разделе https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,SpecialityId,Specialization,Duration,Date,RegNumber,Stage,FormStudy,Budget")] Plan plan)
        {
            if (ModelState.IsValid)
            {
                db.Plans.Edit(plan.Id, plan);
                //  db.Entry(mapper.Convert(planModel)).State = EntityState.Modified;
                //  db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(plan);
        }

        // GET: Plan/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Plan plan = db.Plans.GetItem((int)id);
            if (plan == null)
            {
                return HttpNotFound();
            }
            return View(plan);
        }

        // POST: Plan/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            db.Plans.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
