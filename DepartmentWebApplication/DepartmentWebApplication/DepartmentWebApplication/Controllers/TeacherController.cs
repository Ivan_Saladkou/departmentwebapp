﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccessLayer.Entities;
using System.Web.Mvc;

namespace DepartmentWebApplication.Controllers
{
    public class TeacherController : Controller
    {
        DataBaseDepartment db = new DataBaseDepartment();
        // GET: Teacher
        public ActionResult Index()
        {
            return View(db.Teachers.GetAll());
        }


        // GET: Teacher/create
        public ActionResult Create()
        {
            return View(new Teacher());
        }


        // POST : Teacher/create
        [HttpPost]
        public ActionResult Create([Bind(Include = "Id,FirstName,LastName,MiddleName")] Teacher teacher)
        {
            if (ModelState.IsValid)
            {
                db.Teachers.Add(teacher);
                return RedirectToAction("Index");
            }
            return View(teacher);
        }
    }
}