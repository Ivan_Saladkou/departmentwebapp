﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataAccessLayer;
using DataAccessLayer.Entities;
using DepartmentWebApplication.Models;

namespace DepartmentWebApplication.Controllers
{
    public class SubjectsController : Controller
    {
        private DepartmentEntities db = new DepartmentEntities();
        private DataBaseDepartment db1 = new DataBaseDepartment();

        // GET: Disciplines
        public ActionResult Index()
        {
            var disciplines = db.Subjects;
            return View(disciplines.ToList());
        }

        // GET: Disciplines/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subject discipline = db.Subjects.Find(id);
            if (discipline == null)
            {
                return HttpNotFound();
            }
            return View(discipline);
        }

        // GET: Disciplines/Create
        public ActionResult Create()
        {
            ViewBag.PlanId = new SelectList(db.Plans.Include(e => e.Speciality), "Id", "Speciality.Name");
            Discipline ds = new Discipline();
            ds.Subject = new Subject();
            return View(ds);
        }

        // POST: Disciplines/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в разделе https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Id,Discipline,Discipline.PlanId,Discipline.SequentialNumber,Discipline.Name" +
        //    ",Discipline.NameAbbreviated,Number,Exams,Credits,CourseProjects,CourseWorks,Lectures,Labs,Practices,Seminars,Audits" +
        //    ",Independents,Ksr")] DisciplineSemester disciplineSemester)
        //{
        //    int planId = disciplineSemester.Discipline.PlanId;
        //    if (ModelState.IsValid)
        //    {
        //        int disciplineId = db1.Disciplines.GetAll().Find(d => d.PlanId == planId
        //        && d.Name == disciplineSemester.Discipline.Name)?.Id ?? -1;
        //        if (disciplineId == -1)
        //        {
        //            disciplineId = db1.Disciplines.AddGetId(disciplineSemester.Discipline);
        //        }
        //        disciplineSemester.Discipline = null; //убирает второе добавление

        //        disciplineSemester.DisciplineId = disciplineId;
        //        db1.DisciplineSemesters.Add(disciplineSemester);
        //        return Redirect("/plan/details/" + planId);
        //    }

        //    return View(disciplineSemester);
        //}

        // GET: Disciplines/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subject discipline = db.Subjects.Find(id);
            if (discipline == null)
            {
                return HttpNotFound();
            }
            //ViewBag.PlanId = new SelectList(db.Plans.Include(e => e.Speciality), "Id", "Speciality.Name", discipline.PlanId);
            return View(discipline);
        }

        // POST: Disciplines/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в разделе https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PlanId,SequentialNumber,Name,NameAbbreviated,Exams,Credits,CourseProjects,CourseWorks,Lectures,Labs,Practices,Seminars,Audits,Independents,Ksr")] Subject discipline)
        {
            if (ModelState.IsValid)
            {
                db.Entry(discipline).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.PlanId = new SelectList(db.Plans.Include(e => e.Speciality), "Id", "Speciality.Name", discipline.PlanId);
            return View(discipline);
        }

        // GET: Disciplines/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subject discipline = db.Subjects.Find(id);
            if (discipline == null)
            {
                return HttpNotFound();
            }
            return View(discipline);
        }

        // POST: Disciplines/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Subject discipline = db.Subjects.Find(id);
            db.Subjects.Remove(discipline);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
