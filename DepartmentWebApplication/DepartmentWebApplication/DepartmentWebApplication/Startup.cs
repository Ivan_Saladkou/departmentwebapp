﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DepartmentWebApplication.Startup))]
namespace DepartmentWebApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
