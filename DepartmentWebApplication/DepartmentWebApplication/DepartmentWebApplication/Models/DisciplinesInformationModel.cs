﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DepartmentWebApplication.Models
{
    public class DisciplinesInformationModel
    {
        public string[] DisciplinesInformationPaths { get; set; }
        public string ErrorMessage { get; set; }
    }
}