﻿function WriteAbbreviation(element, text) {
    element.value = text.split(' ').map(x => x[0].toUpperCase()).join('');
}

function saveContingents() {
    var table = document.getElementsByTagName('table')[0];
    var rows = [];
    [].push.apply(rows, table.getElementsByTagName('tr'));
    var contingents = rows.reduce(function (array, row) {
        if (row.id[0] === 'c' || row.id[0] === 's') { //c-contingent | s-speciality, цифра после - id в бд
            var contingent = {};
            if (row.id[0] === 'c') {
                contingent.id = row.id.slice(1);
            } else {
                contingent.specialityId = row.id.slice(1);
            }
            var courses = [];
            var inputs = row.getElementsByTagName('input');
            for (var i = 0; i < 5; i++) {
                var course = {};
                course.number = i + 1;
                course.budget = inputs[i * 5].value;
                course.offBudget = inputs[i * 5 + 1].value;
                course.foreign = inputs[i * 5 + 2].value;
                course.budgetGroups = inputs[i * 5 + 3].value;
                course.offBudgetGroups = inputs[i * 5 + 4].value;
                course.contingentId = contingent.id;
                courses.push(course);
            }
            contingent.courses = courses;
            array.push(contingent);
        }
        return array;
    }, []);

    var json = JSON.stringify(contingents);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "");
    xhr.send(json);
}