﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccessLayer.Repositories;

namespace DepartmentWebApplication
{
    public class DataBaseDepartment
    {
        public DisciplineRepository Disciplines { get; } = new DisciplineRepository();
        public PlanRepository Plans { get; } = new PlanRepository();
        public SpecialityRepository Specialities { get; } = new SpecialityRepository();
        public ContingentRepository Contingents { get; } = new ContingentRepository();
        public CourseRepository Courses { get; } = new CourseRepository();
        public TeacherRepository Teachers { get; } = new TeacherRepository();
        public DisciplineSemesterRepository DisciplineSemesters { get; } = new DisciplineSemesterRepository();
    }
}