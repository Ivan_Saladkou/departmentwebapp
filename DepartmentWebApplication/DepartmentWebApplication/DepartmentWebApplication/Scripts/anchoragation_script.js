function moveDiscipline() {

    let disciplines = document.getElementById("Disciplines");
    let departments = document.getElementById("Departments");
    let selectedDepartment = Array.from(departments.options)
        .filter(option => option.selected)[0];

    if (selectedDepartment == undefined || selectedDepartment.innerHTML.startsWith("--")) {
        document.getElementById("error-message").classList.remove('d-none');
       // document.getElementById("error-message").innerHTML = ("Выберите кафедру");
        return;
    }

    let selectedDisciplines = Array.from(disciplines.options)
        .filter(option => option.selected);
    selectedDisciplines.forEach(option => {
        option.innerHTML = "--" + option.innerHTML;
        selectedDepartment.after(option)

    });
}

function moveDisciplineBack() {
    let departments = document.getElementById("Departments");
    Array.from(departments.options)
        .filter(option => option.selected)
        .forEach(option => moveIfValid(option))
}

function moveIfValid(option) {
    if (option.innerHTML.startsWith("--")) {
        option.innerHTML = option.innerHTML.substring(2);
        let planId = option.value.split("/")[1];
        let plan = document.getElementById("p_" + planId);
        console.log(planId);
        plan.prepend(option);
    }
}

function clearErrorMessage() {
    document.getElementById("error-message").classList.add('d-none');

   // document.getElementById("error-message").innerHTML = ("");
}

function setSelected() {
    let departmentsSelect = document.getElementById("Departments");
    let departmentOptions = Array.from(departmentsSelect.options);
    departmentOptions.forEach(department => department.selected = "selected");

    let disciplineSelect = document.getElementById("Disciplines");
    let disciplineOptions = Array.from(disciplineSelect.options);
    disciplineOptions.forEach(discipline => discipline.selected = "selected")
}







