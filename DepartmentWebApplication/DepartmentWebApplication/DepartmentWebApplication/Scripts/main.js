﻿function showInfoBlock(){
    infoBlock = $("#info-block")[0];
    infoBlock.style.display = "block";
}

function menu(btn) {
    let navig = document.getElementsByClassName('navigation')[0]
    if (btn.dataset.is_active == '0') {
        btn.dataset.is_active = 1
        navig.style.display = 'flex';
        btn.style.transform = 'rotate(90deg)';
    } else {
        navig.style.display = 'none';
        btn.dataset.is_active = 0;
        btn.style.transform = 'rotate(0)'

    }
}

function filter(btn) {
    let navig = document.getElementsByClassName('distribution__discipline-filter-list')[0]
    let icon = btn.getElementsByTagName('p')[0];
    if (btn.dataset.is_active == '0') {
        btn.dataset.is_active = 1
        navig.style.transform = 'scale(1, 1)';
        icon.style.transform = 'rotate(180deg)'
    } else {
        navig.style.transform = 'scale(1, 0)';
        btn.dataset.is_active = 0;
        icon.style.transform = 'rotate(0deg)'

    }
}

function set_filter(li) {
    let navig = document.getElementsByClassName('distribution__discipline-filter-list')[0]
    let btn = document.getElementsByClassName('distribution__discipline-filter-btn')[0];
    let span = btn.getElementsByTagName('span')[0];
    let icon = btn.getElementsByTagName('p')[0];
    span.innerHTML = li.innerHTML;
    btn.dataset.is_active = '0';
    navig.style.transform = 'scale(1, 0)';
    icon.style.transform = 'rotate(0deg)';
}


function changeValue(event, btn, isIncrease) {
    let pointNum = btn.dataset.pointnum;
    let teacher = $('select[name=teacherId]').val();
    let distribution = $('select[name=distributionId]').val();
    $.ajax({
        url: window.location.href + '/ChangeValue',
        data: {
            teacherId: +teacher,
            distributionId: +distribution,
            isIncrease: isIncrease,
            pointNum: +pointNum,
            isAll: event.ctrlKey
        },
        complete: function(data) {
            $('#searchInfo').html(data.responseText);
        },
        async: false,
        method: "post"
    });
}

function closeInfoBlock(btn){
    block = btn.parentElement;
    block.style.display = "none"
}
