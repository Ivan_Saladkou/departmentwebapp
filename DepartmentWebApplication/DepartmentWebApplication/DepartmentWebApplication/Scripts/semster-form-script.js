var arr = new Array();
let size = document.getElementsByClassName("semester").length;
for (let i = 1; i <= size; i++) {
  arr[i - 1] = i;
}

function referesh() {
  var arr = new Array();
  let size = document.getElementsByClassName("semester").length;
  deleteAllSemesters();
  for (let i = 1; i <= size; i++) {
    arr[i - 1] = i;
    addSemester(i);
  }
}

function deleteAllSemesters() {
  let semesters = Array.from(document.getElementsByClassName("semester"));
  semesters.forEach(element => element.remove());
}

function addSemester(number) {
  let add = document.getElementById("add");
  let semester = createSemester(number);
  add.before(semester);
}

function addSemesterToEnd() {
  let lastSemester = arr[arr.length - 1];
  addSemester(lastSemester + 1);
  arr.push(lastSemester + 1);
}

function createSemester(number) {
  let semester = document.createElement("div");
    semester.className = "semester";
  semester.innerHTML = "<span>" + number +
    ' семестр </span><input name="semesters" type="number" value="17"> ' +
    '<span>недель</span>  <label onclick="deleteSemester(this)">-</label>';
  return semester;
}

function deleteSemester(node) {
  node.parentElement.remove();
  arr.pop();
  referesh();
}
