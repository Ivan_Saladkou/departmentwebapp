﻿
var bySemesters = document.getElementsByClassName('by-semester');
var byFormOfStudy = document.getElementsByClassName('by-form-of-study');
var formButton = document.getElementById('form-btn');
var semestersButton = document.getElementById('semesters-btn');

function sortBySemesters() {
    changeView(bySemesters, byFormOfStudy, semestersButton, formButton);

}

function sortByForm() {
    changeView(byFormOfStudy, bySemesters, formButton, semestersButton);
}


function changeView(toShow, toHide, clickedBtn, otherBtn) {
    for (let i = 0; i < toHide.length; i++) {
        toHide[i].setAttribute('style', 'display:none;');
    }
    for (let i = 0; i < bySemesters.length; i++) {
        toShow[i].removeAttribute('style');
    }
    clickedBtn.setAttribute('disabled', 'disabled');
    otherBtn.removeAttribute('disabled');
}
