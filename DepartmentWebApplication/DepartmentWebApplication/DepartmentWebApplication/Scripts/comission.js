﻿function isGraduationProject() {
    var list = document.getElementById('SubjectId');
    var title = list.options[list.selectedIndex].text;
    var commission = document.getElementById('commission');
    var commissionCheckbox = document.getElementById('commission-checkbox');
    var basic = document.getElementById('basic');
    if (title === 'Дипломное проектирование') {
        commission.classList.remove('hidden');
        commissionCheckbox.checked = false;
        isWithCommisssion();
        basic.classList.add('hidden');
    } else {
        commission.classList.add('hidden');
        basic.classList.remove('hidden');
        
    }
}

function isWithCommisssion() {
    var commissionCheckbox = document.getElementById('commission-checkbox');
    var memberAmount = document.getElementById('member-amount');
    if (commissionCheckbox.checked) {
        memberAmount.classList.remove('hidden');
    } else {
        memberAmount.classList.add('hidden');
        var input = document.getElementById('member-amount-input');
        input.value = 0;
    }
}