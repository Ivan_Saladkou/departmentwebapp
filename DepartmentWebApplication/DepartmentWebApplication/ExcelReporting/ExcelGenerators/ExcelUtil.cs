﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace ExcelReporting.ExcelGenerators
{
    public static class ExcelUtil
    {
        private static char[] columnNames = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
            'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

        private static Color colorYellow = Color.Yellow;
        private static Color colorGreen = Color.FromArgb(146, 208, 80);

        public static char GetColumnName(int columnId)
        {
            return columnNames[columnId - 1];
        }

        public static void AddHeaderHoursNames(ExcelWorksheet sheet, string header)
        {
            int rowId = 1;
            const int columnIdSum = 17;

            sheet.Cells[rowId, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            sheet.Cells[rowId, 2].Value = $"I.Учебная работа (выполнение {header})";
            sheet.Cells[rowId, 2, rowId, 12].Merge = true;

            rowId++;
            sheet.Cells[rowId, 1].Value = "Преподаватель";
            sheet.Cells[rowId, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
            sheet.Cells[rowId, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            sheet.Cells[rowId, 1, rowId + 1, 1].Merge = true;

            sheet.Cells[rowId, 2].Value = "Количество часов";
            sheet.Cells[rowId, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            sheet.Cells[rowId, 2, rowId, columnIdSum - 1].Merge = true;

            sheet.Cells[rowId, columnIdSum].Value = "Всего уч. Часов";
            sheet.Cells[rowId, columnIdSum].Style.TextRotation = 90;
            sheet.Cells[rowId, columnIdSum].Style.WrapText = true;
            sheet.Cells[rowId, columnIdSum, rowId + 1, columnIdSum].Merge = true;
            sheet.Cells[rowId, columnIdSum, rowId + 1, columnIdSum].Style.Font.Size = 12;

            rowId++;
            sheet.Row(rowId).Height = 100;

            sheet.Cells[rowId, 2].Value = "Лекции";
            sheet.Cells[rowId, 3].Value = "Кол-во лаб. часов всего";
            sheet.Cells[rowId, 4].Value = "Кол-во пр. часов всего";
            sheet.Cells[rowId, 5].Value = "Модульно-рейтинговая система";
            sheet.Cells[rowId, 6].Value = "Инд. Задания";
            sheet.Cells[rowId, 7].Value = "Курсовое проектирование";
            sheet.Cells[rowId, 8].Value = "Консультации";
            sheet.Cells[rowId, 9].Value = "Рецензирование";
            sheet.Cells[rowId, 10].Value = "Зачеты";
            sheet.Cells[rowId, 11].Value = "Экзамены";
            sheet.Cells[rowId, 12].Value = "Учебная практика";
            sheet.Cells[rowId, 13].Value = "Производственная практика";
            sheet.Cells[rowId, 14].Value = "Дипломное проектирование";
            sheet.Cells[rowId, 15].Value = "ГЭК / ГАК";
            sheet.Cells[rowId, 16].Value = "Аспирантура";
            sheet.Cells[rowId, 2, rowId, columnIdSum - 1].Style.TextRotation = 90;
            sheet.Cells[rowId, 2, rowId, columnIdSum - 1].Style.WrapText = true;
            sheet.Cells[rowId, 2, rowId, columnIdSum - 1].Style.Font.Size = 12;

            sheet.View.FreezePanes(4, 1);
            AddAllBorders(sheet.Cells[rowId - 1, 1, rowId, columnIdSum], ExcelBorderStyle.Medium);
        }

        public static void AddAllBorders(ExcelRangeBase range, ExcelBorderStyle borderStyle)
        {
            range.Style.Border.Left.Style = borderStyle;
            range.Style.Border.Top.Style = borderStyle;
            range.Style.Border.Right.Style = borderStyle;
            range.Style.Border.Bottom.Style = borderStyle;
        }

        public static string FormulaSumRange(int rowId1, int columnId1, int rowId2, int columnId2)
        {
            return $"SUM({columnNames[columnId1 - 1]}{rowId1}:{columnNames[columnId2 - 1]}{rowId2})";
        }

        public static void AddHeaderDone(ExcelWorksheet sheet, int columnId)
        {
            const int rowId = 2;

            sheet.Cells[rowId, columnId].Value = "Недовыполнено";
            sheet.Cells[rowId, columnId + 1].Value = "Перевыполнено";

            sheet.Cells[rowId, columnId, rowId + 1, columnId].Merge = true;
            sheet.Cells[rowId, columnId + 1, rowId + 1, columnId + 1].Merge = true;

            sheet.Cells[rowId, columnId, rowId, columnId + 1].Style.TextRotation = 90;
            sheet.Cells[rowId, columnId, rowId + 1, columnId + 1].Style.Font.Size = 12;
            AddAllBorders(sheet.Cells[rowId, columnId, rowId + 1, columnId + 1], ExcelBorderStyle.Medium);
        }

        public static void AddSign(ExcelWorksheet sheet, int rowId)
        {
            sheet.Cells[rowId, 1].Value = "Зав. кафедрой";
        }

        public static void AddConditionProgressPlan(ExcelWorksheet sheet, string address, string formula)
        {
            var conditionAddress = sheet.Cells[address];
            var conditionGreater = sheet.ConditionalFormatting.AddGreaterThan(conditionAddress);
            conditionGreater.Formula = formula;
            conditionGreater.Style.Fill.BackgroundColor.Color = colorGreen;

            var conditionLess = sheet.ConditionalFormatting.AddLessThan(conditionAddress);
            conditionLess.Formula = formula;
            conditionLess.Style.Fill.BackgroundColor.Color = colorYellow;
        }
    }
}
