﻿using ExcelReporting.Enums;
using ExcelReporting.Models;
using OfficeOpenXml;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ExcelReporting.ExcelGenerators
{
    public class IndividualPlanGenerator
    {
        private string savePath;
        private string tempPath;

        public IndividualPlanGenerator(string savePath, string tempPath)
        {
            this.savePath = savePath;
            this.tempPath = tempPath;
        }

        public void Generate(IEnumerable<EmployeeIndividual> employees)
        {
            using (var excel = new ExcelPackage(new FileInfo(tempPath)))
            {
                ExcelWorksheet worksheet = excel.Workbook.Worksheets["План"];
                foreach (var employee in employees)
                {
                    worksheet.Cells[4, 3, 18, 22].Value = "";
                    worksheet.Cells[21, 3, 35, 22].Value = "";

                    FillSemester(worksheet, employee.Disciplines.Where(e => e.Semester == Semester.Autumn), 4);
                    FillSemester(worksheet, employee.Disciplines.Where(e => e.Semester == Semester.Spring), 21);
                    excel.SaveAs(new FileInfo($@"{savePath}\{employee.Name}.xlsx"));
                    //File.WriteAllBytes($@"{savePath}\{employee.Name}.xlsx", excel.Stream.());
                }
            }
        }

        private void FillSemester(ExcelWorksheet worksheet, IEnumerable<DisciplineIndividual> disciplines, int rowId)
        {
            foreach (var discipline in disciplines.Take(15))
            {
                worksheet.Cells[rowId, 3].Value = discipline.Name;
                worksheet.Cells[rowId, 6].Value = discipline.Course;
                for (int col = 0; col < discipline.Hours.Count(); col++)
                {
                    worksheet.Cells[rowId, 8 + col].Value = discipline.Hours[col];
                }
                rowId++;
            }
        }
    }
}
