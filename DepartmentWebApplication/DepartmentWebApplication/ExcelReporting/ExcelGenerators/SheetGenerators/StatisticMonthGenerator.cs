﻿using ExcelReporting.Models;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Collections.Generic;

namespace ExcelReporting.ExcelGenerators.SheetGenerators
{
    public class StatisticMonthGenerator
    {
        private const int rowIdStart = 5;
        private const int hoursCount = 15;
        private const int columnIdSum = hoursCount + 2;
        private const string fontName = "Times New Roman";

        private ExcelPackage excel;
        private List<EmployeeHours> employees;

        public StatisticMonthGenerator(ExcelPackage excel, List<EmployeeHours> employees)
        {
            this.excel = excel;
            this.employees = employees;
        }

        public void Generate(string month)
        {
            int rowId = rowIdStart;
            ExcelWorksheet sheet = excel.Workbook.Worksheets.Add(month);
            ExcelUtil.AddHeaderHoursNames(sheet, month);
            double[] employeeHours;
            foreach (EmployeeHours employee in employees)
            {
                sheet.Cells[rowId, 1].Value = employee.Name;
                employeeHours = employee.GetHoursDone(month);
                for (int column = 0; column < hoursCount; column++)
                {
                    sheet.Cells[rowId, column + 2].Value = employeeHours[column];
                }
                sheet.Cells[rowId, columnIdSum].Formula = ExcelUtil.FormulaSumRange(rowId, 2, rowId, hoursCount + 1);
                rowId++;
            }

            rowId++;
            sheet.Cells[rowId, 1].Value = "Всего выполнено за " + month.ToLower();
            for (int column = 0; column < hoursCount; column++)
            {
                sheet.Cells[rowId, column + 2].Formula = ExcelUtil.FormulaSumRange(rowIdStart, column + 2, rowId - 1, column + 2);
            }
            sheet.Cells[rowId, columnIdSum].Formula = ExcelUtil.FormulaSumRange(rowId, 2, rowId, hoursCount + 1);
            ExcelUtil.AddAllBorders(sheet.Cells[rowIdStart - 1, 1, rowId, columnIdSum], ExcelBorderStyle.Thin);
            sheet.Cells.Style.Font.Name = fontName;
            ExcelUtil.AddSign(sheet, rowId + 3);
            sheet.Column(1).AutoFit();
        }
    }
}
