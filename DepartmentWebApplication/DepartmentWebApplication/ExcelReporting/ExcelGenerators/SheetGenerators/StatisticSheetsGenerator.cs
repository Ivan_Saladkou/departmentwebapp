﻿using ExcelReporting.Enums;
using ExcelReporting.Extensions;
using ExcelReporting.Models;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Collections.Generic;
using System.Linq;

namespace ExcelReporting.ExcelGenerators.SheetGenerators
{
    public class StatisticSheetsGenerator
    {
        private const int rowIdStart = 5;
        private const int hoursCount = 15;
        private const int columnIdSum = hoursCount + 2;
        private const string formulaIfHours = "IF((Q{0}-Q{1})<0,(Q{1}-Q{0}),\"\")";
        private const string addressCondition = "B{0}:Q{0}";
        private const string fontName = "Times New Roman";

        private ExcelPackage excel;
        private List<EmployeeHours> employees;

        public StatisticSheetsGenerator(ExcelPackage excel, List<EmployeeHours> employees)
        {
            this.excel = excel;
            this.employees = employees;
        }

        public void Generate(string[] sheetNames, string sheetSaveName, StudyPeriod period, int rowStep)
        {
            int rowId = rowIdStart;
            string periodName = period.GetDisplayName();
            string namePlanned = "Запланировано на " + periodName;

            ExcelWorksheet sheet = excel.Workbook.Worksheets.Add(sheetSaveName);

            ExcelUtil.AddHeaderHoursNames(sheet, periodName);
            ExcelUtil.AddHeaderDone(sheet, hoursCount + 3);
            string formulaSumSheets = string.Join(" + ", sheetNames.Select(e => e + "!{0}"));

            for (int i = 0; i < employees.Count; i++)
            {
                sheet.Row(rowId + 1).OutlineLevel = 1;
                sheet.Cells[rowId, 1].Value = employees[i].Name;
                sheet.Cells[rowId + 1, 1].Value = namePlanned;

                string rowIdOtherSheetString = (rowIdStart + i * rowStep).ToString();

                double[] employeeHoursPlanned = employees[i].GetHoursPlanned(period);

                for (int column = 0; column < hoursCount; column++)
                {
                    sheet.Cells[rowId, column + 2].Formula
                        = string.Format(formulaSumSheets, ExcelUtil.GetColumnName(column + 2) + rowIdOtherSheetString);
                    sheet.Cells[rowId + 1, column + 2].Value = employeeHoursPlanned[column];
                }

                ExcelUtil.AddConditionProgressPlan(sheet, string.Format(addressCondition, rowId), "B" + (rowId + 1));

                sheet.Cells[rowId, columnIdSum].Formula = ExcelUtil.FormulaSumRange(rowId, 2, rowId, hoursCount + 1);
                sheet.Cells[rowId + 1, columnIdSum].Formula = ExcelUtil.FormulaSumRange(rowId + 1, 2, rowId + 1, hoursCount + 1);

                sheet.Cells[rowId, hoursCount + 3].Formula = string.Format(formulaIfHours, rowId, rowId + 1);
                sheet.Cells[rowId, hoursCount + 4].Formula = string.Format(formulaIfHours, rowId + 1, rowId);
                rowId += 2;
            }
            ExcelUtil.AddAllBorders(sheet.Cells[rowIdStart - 1, 1, rowId - 1, columnIdSum], ExcelBorderStyle.Thin);

            rowId++;
            sheet.Cells[rowId, 1].Value = "Всего выполнено за " + periodName;
            sheet.Cells[rowId + 1, 1].Value = "Запланировано на " + periodName;

            int[] sequenceNumbers = Enumerable.Range(0, employees.Count).ToArray();

            string formulaSumPlanRow = string.Join(" + ", sequenceNumbers.Select(e => "{0}" + (rowIdStart + e * 2)));
            string formulaSumDoneRow = string.Join(" + ", sequenceNumbers.Select(e => "{0}" + (rowIdStart + e * 2 + 1)));

            ExcelUtil.AddAllBorders(sheet.Cells[rowId, 1, rowId + 1, columnIdSum], ExcelBorderStyle.Thin);
            ExcelUtil.AddConditionProgressPlan(sheet, string.Format(addressCondition, rowId), "B" + (rowId + 1));


            for (int column = 0; column < hoursCount; column++)
            {
                sheet.Cells[rowId, column + 2].Formula
                    = string.Format(formulaSumPlanRow, ExcelUtil.GetColumnName(column + 2));
                sheet.Cells[rowId + 1, column + 2].Formula
                    = string.Format(formulaSumDoneRow, ExcelUtil.GetColumnName(column + 2));
            }

            sheet.Cells[rowId, columnIdSum].Formula = ExcelUtil.FormulaSumRange(rowId, 2, rowId, hoursCount + 1);
            sheet.Cells[rowId + 1, columnIdSum].Formula = ExcelUtil.FormulaSumRange(rowId + 1, 2, rowId + 1, hoursCount + 1);

            sheet.Cells[rowId, hoursCount + 3].Formula = string.Format(formulaIfHours, rowId, rowId + 1);
            sheet.Cells[rowId, hoursCount + 4].Formula = string.Format(formulaIfHours, rowId + 1, rowId);

            sheet.Cells.Style.Font.Name = fontName;
            ExcelUtil.AddSign(sheet, rowId + 4);
            sheet.Column(1).AutoFit();
        }
    }
}