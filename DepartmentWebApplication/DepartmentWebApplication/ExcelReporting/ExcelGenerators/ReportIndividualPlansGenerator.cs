﻿using ExcelReporting.Enums;
using ExcelReporting.ExcelGenerators.SheetGenerators;
using ExcelReporting.Extensions;
using ExcelReporting.Models;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ExcelReporting.ExcelGenerators
{
    public class ReportIndividualPlansGenerator
    {
        private const string fulfillmentPage = "Выполнение";

        private List<EmployeeHours> employees = new List<EmployeeHours>();
        private List<string> monthNames = new List<string>(10);

        private const int rowIdSeptember = 4;
        private const int rowIdFebruary = 21;
        private const int rowIdPlannedFirst = 20;
        private const int rowIdPlannedSecond = 37;
        private const int hoursCount = 15;

        private readonly int[] rowIdMonths = { rowIdSeptember, rowIdSeptember + 3, rowIdSeptember + 6, rowIdSeptember + 9, rowIdSeptember + 12,
            rowIdFebruary, rowIdFebruary + 3, rowIdFebruary + 6, rowIdFebruary + 9, rowIdFebruary + 12 };

        public ReportIndividualPlansGenerator()
        {
            foreach (StudyMonth month in Enum.GetValues(typeof(StudyMonth)))
            {
                monthNames.Add(month.GetDisplayName());
            }
        }

        public void Generate(string plansPath, string savePath, string fileName)
        {
            string[] planPaths = Directory.GetFiles(plansPath, "*.xlsx");

            Parallel.ForEach(planPaths, path =>
            {
                using (var plan = new ExcelPackage(new FileInfo(path)))
                {
                    EmployeeHours employee = new EmployeeHours(plan.File.Name.Replace(plan.File.Extension, ""));
                    ExcelWorksheet sheet = plan.Workbook.Worksheets[fulfillmentPage];
                    for (int im = 0; im < monthNames.Count; im++)
                    {
                        employee.AddHoursDone(monthNames[im], GetHours(sheet, rowIdMonths[im]));
                    }
                    employee.SetHoursPlanned(0, GetHours(sheet, rowIdPlannedFirst));
                    employee.SetHoursPlanned(1, GetHours(sheet, rowIdPlannedSecond));
                    employees.Add(employee);
                }
            });

            using (ExcelPackage statistic = new ExcelPackage())
            {
                var statisticMonth = new StatisticMonthGenerator(statistic, employees);
                var statisticSheets = new StatisticSheetsGenerator(statistic, employees);

                string[] firstHalfMonths = monthNames.Take(5).ToArray();
                string[] secondHalfMonths = monthNames.Skip(5).ToArray();

                foreach (var month in firstHalfMonths)
                {
                    statisticMonth.Generate(month);
                }
                const string firstHalfYear = "ПервоеПолугодие";
                statisticSheets.Generate(firstHalfMonths, firstHalfYear, StudyPeriod.FirstHalf, 1);

                foreach (var month in secondHalfMonths)
                {
                    statisticMonth.Generate(month);
                }
                const string secondHalfYear = "ВтороеПолугодие";
                statisticSheets.Generate(secondHalfMonths, secondHalfYear, StudyPeriod.SecondHalf, 1);
                statisticSheets.Generate(new[] { firstHalfYear, secondHalfYear }, "ЗаГод", StudyPeriod.Year, 2);

                File.WriteAllBytes($"{savePath}\\{fileName}.xlsx", statistic.GetAsByteArray());
            }
        }

        private double[] GetHours(ExcelWorksheet sheet, int rowId)
        {
            double[] hours = new double[hoursCount];
            for (int column = 0; column < hoursCount; column++)
            {
                sheet.Cells[rowId, 8 + column].Calculate();
                hours[column] = sheet.Cells[rowId, 8 + column].GetValue<double>();
            }
            return hours;
        }
    }
}