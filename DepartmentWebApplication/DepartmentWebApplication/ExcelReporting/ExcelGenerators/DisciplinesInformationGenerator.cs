﻿using ExcelReporting.Models;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReporting.ExcelGenerators
{
    public class DisciplinesInformationGenerator
    {
        private string savePath;
        private string tempPath;
        private string saveName;
        private const string sheetName = "Лист1";

        public DisciplinesInformationGenerator(string savePath, string tempPath, string saveName)
        {
            this.savePath = savePath;
            this.tempPath = tempPath;
            this.saveName = saveName;
        }

        public void Generate<T>(IEnumerable<IGrouping<T, DisciplineIndividual>> groups)
        {
            using (var excel = new ExcelPackage(new FileInfo(tempPath)))
            {
                var sheet = excel.Workbook.Worksheets[sheetName];
                int rowId = 6;
                int number;

                foreach (IGrouping<T, DisciplineIndividual> group in groups)
                {
                    number = 1;
                    foreach (IGrouping<int, DisciplineIndividual> disciplinesByCourse in group.OrderBy(e => e.Course).GroupBy(e => e.Course))
                    {
                        sheet.Cells[rowId, 2].Value = $"Курс {disciplinesByCourse.Key}";
                        rowId += 2;
                        foreach (DisciplineIndividual discipline in disciplinesByCourse)
                        {
                            sheet.Cells[rowId, 1].Value = number++;
                            GenerateDiscipline(sheet, discipline, ref rowId);
                        }
                    }
                    rowId += 2;
                }

                File.WriteAllBytes($"{savePath}\\{saveName}.xlsx", excel.GetAsByteArray());
            }
        }

        private void GenerateDiscipline(ExcelWorksheet sheet, DisciplineIndividual discipline, ref int rowId)
        {
            sheet.Cells[rowId, 2].Value = discipline.Name;
            if (discipline.BudgetPeoples != null && discipline.BudgetPeoples.Length != 0 && discipline.BudgetPeoples[0] != null &&
                discipline.BudgetPeoples[0].Lecture != null)
            {
                sheet.Cells[rowId, 4].Value = MyConverter.Around(discipline.Plan.Lectures, 17);
                sheet.Cells[rowId, 6].Value = $"{discipline.BudgetPeoples[0].Lecture.Position.ToLower()}, {discipline.BudgetPeoples[0].Lecture.Name}";
            }
            int labRow = 0, practriceRow = 0;

            for (int i = 0; i < discipline.BudgetCount; i++)
            {
                if (discipline.BudgetPeoples[i].Labs != null)
                {
                    sheet.Cells[rowId + labRow, 7].Value = MyConverter.Around(discipline.Plan.Labs, 17);
                    sheet.Cells[rowId + labRow++, 9].Value = $"{discipline.BudgetPeoples[i].Labs.Position.ToLower()}, {discipline.BudgetPeoples[i].Labs.Name}";
                }
                if (discipline.BudgetPeoples[i].Practice != null)
                {
                    sheet.Cells[rowId + practriceRow, 10].Value = MyConverter.Around(discipline.Plan.Practices, 17);
                    sheet.Cells[rowId + practriceRow++, 12].Value = $"{discipline.BudgetPeoples[i].Practice.Position.ToLower()}, {discipline.BudgetPeoples[i].Practice.Name}";
                }
            }
            for (int i = 0; i < discipline.FeeCount; i++)
            {
                if (discipline.FeePeoples[i].Labs != null)
                {
                    sheet.Cells[rowId + labRow, 7].Value = MyConverter.Around(discipline.Plan.Labs, 17);
                    sheet.Cells[rowId + labRow++, 9].Value = $"{discipline.FeePeoples[i].Labs.Position.ToLower()}, {discipline.FeePeoples[i].Labs.Name}";
                }
                if (discipline.FeePeoples[i].Practice != null)
                {
                    sheet.Cells[rowId + practriceRow, 10].Value = MyConverter.Around(discipline.Plan.Practices, 17);
                    sheet.Cells[rowId + practriceRow++, 12].Value = $"{discipline.FeePeoples[i].Practice.Position.ToLower()}, {discipline.FeePeoples[i].Practice.Name}";
                }
            }
            for (int r = 0; r < (discipline.BudgetCount + discipline.FeeCount + 1) / 2; r++)
                sheet.Cells[rowId + r * 2, 3].Value = $"{discipline.Course}-курс группа {r + 1}";

            rowId += (discipline.BudgetCount + discipline.FeeCount + 1) / 2 * 2;
        }
    }
}
