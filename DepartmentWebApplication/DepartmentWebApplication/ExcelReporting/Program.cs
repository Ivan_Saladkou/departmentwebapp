﻿using DataAccessLayer;
using DataAccessLayer.Entities;
using DataAccessLayer.Enums;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReporting
{
    class Program
    {

        static void Main(string[] args)
        {
            StateDocument state;
            using (DepartmentEntities db = new DepartmentEntities())
            {
                state = db.StateDocumentes
             .Include(s => s.Department)
             .Include(s => s.StateBlocks
                 .Select(b => b.StateRows))
             .First(s => s.Id == 12);
            }

            var package = new ExcelPackage();
            package.Load(File.OpenRead("../../States/StateTemplate.xlsx"));
            var worksheet = package.Workbook.Worksheets.First();

            worksheet.Cells["I1"].Value = "\"" + state.Department.Name + "\"";
            worksheet.Cells["M1"].Value = (state.Budget == Budget.Rb) ? "Белорусским" : "Российским";
            worksheet.Cells["T1"].Value = state.Date.Year + "-" + (state.Date.Year + 1);
            string date = state.FormationDate.ToString("dd/MM/yy_hh/mm/ss");

            int counter = 1;
            int rowNumber = 10;
            int columnNumber = 1;

            foreach (StateBlock block in state.StateBlocks)
            {
                string stage = (block.Stage == Stage.First) ? "I" : "II";
                string timeOfYear = (block.TimeOfYear == TimeOfYear.Autumn) ? "Осенний" : "Весенний";
                string formOfStudy = (block.FormStudy == FormStudy.FullTime) ? "Очное" : "Заочное";
                worksheet.Cells["K" + rowNumber++].Value = "Студенты " + stage + " ступени образования " + timeOfYear + " семестр " + formOfStudy + " отделение";

                foreach (StateRow row in block.StateRows.Where(r => !block.StateExaminationComissionDisciplines.Contains(r)))
                {
                    worksheet.Cells[rowNumber, columnNumber].Value = counter;
                    DrawRow(row, worksheet, rowNumber, columnNumber);
                    rowNumber += 2;
                    columnNumber = 1;
                    counter++;
                }
                if (block.TimeOfYear == TimeOfYear.Spring && block.StateExaminationComissionDisciplines.Count != 0)
                {
                    worksheet.Cells["K" + rowNumber++].Value = "ГЭК / ГАК, Дипломное проектирование";
                    foreach (StateRow row in block.StateRows.Where(r => block.StateExaminationComissionDisciplines.Contains(r)))
                    {
                        worksheet.Cells[rowNumber, columnNumber].Value = counter;
                        DrawDiplomaRow(row, worksheet, rowNumber, columnNumber);
                        rowNumber += 2;
                        columnNumber = 1;
                        counter++;
                    }
                }
                worksheet.Cells[rowNumber, columnNumber].Value = "Итого по " + ((block.FormStudy == FormStudy.FullTime) ? " дневному" : " заочному") + " отделению";
                DrawBlockTotal(block, worksheet, rowNumber, columnNumber);
                rowNumber += 2;
                worksheet.Cells[rowNumber, columnNumber].Value = "Всего по " + ((block.FormStudy == FormStudy.FullTime) ? " дневному" : " заочному") + " отделению";
                DrawBlockSum(block, worksheet, rowNumber, columnNumber);

                rowNumber += 3;
                columnNumber = 1;

            }

            File.WriteAllBytes("../../States/State_" + date + ".xlsx", package.GetAsByteArray());


        }

        private static void MergeWithUnderneath(ExcelWorksheet worksheet, int rowNumber, int columnNumber)
        {
            worksheet.Cells[rowNumber, columnNumber, rowNumber + 1, columnNumber].Merge = true;
        }

        private static void DrawBorder(ExcelWorksheet worksheet, int fstRowNumber, int fstColumnNumber, int sndRowNumber, int sndColumnNumber)
        {

            for (int i = fstRowNumber; i <= sndRowNumber; i++)
            {
                for (int j = fstColumnNumber; j <= sndColumnNumber; j++)
                {
                    worksheet.Cells[i, j].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                }
            }
        }

        private static void DrawBlockSum(StateBlock block, ExcelWorksheet worksheet, int rowNumber, int columnNumber)
        {
            int startRowNumber = rowNumber;
            int startColumnNumber = columnNumber;

            worksheet.Cells[rowNumber, columnNumber, rowNumber + 1, columnNumber + 2].Merge = true;
            worksheet.Cells[rowNumber, columnNumber + 3, rowNumber + 1, columnNumber + 6].Merge = true;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber + 7);

            columnNumber = 9;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber);
            worksheet.Cells[rowNumber, columnNumber++].Value = block.Lectures;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber++);
            MergeWithUnderneath(worksheet, rowNumber, columnNumber);
            worksheet.Cells[rowNumber, columnNumber++].Value = block.LabsTotal;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber++);
            MergeWithUnderneath(worksheet, rowNumber, columnNumber);
            worksheet.Cells[rowNumber, columnNumber++].Value = block.PracticeTotal;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber);
            worksheet.Cells[rowNumber, columnNumber++].Value = block.ModuleRatingSystemTotal;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber++);
            MergeWithUnderneath(worksheet, rowNumber, columnNumber);
            worksheet.Cells[rowNumber, columnNumber++].Value = block.CourseProjectTotal;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber);
            worksheet.Cells[rowNumber, columnNumber++].Value = block.ConsultationsTotal;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber);
            worksheet.Cells[rowNumber, columnNumber++].Value = block.ReviewingTestsTotal;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber);
            worksheet.Cells[rowNumber, columnNumber++].Value = block.OffsetsTotal;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber);
            worksheet.Cells[rowNumber, columnNumber++].Value = block.ExamsTotal;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber);
            worksheet.Cells[rowNumber, columnNumber++].Value = block.StudyPracticeTotal;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber);
            worksheet.Cells[rowNumber, columnNumber++].Value = block.ProductionPracticeTotal;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber);
            worksheet.Cells[rowNumber, columnNumber++].Value = block.ThesisProjectTotal;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber);
            worksheet.Cells[rowNumber, columnNumber++].Value = block.StateExaminationComission;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber);
            worksheet.Cells[rowNumber, columnNumber++].Value = "-";
            MergeWithUnderneath(worksheet, rowNumber, columnNumber);
            worksheet.Cells[rowNumber, columnNumber].Value = block.Total;
            rowNumber++;
            DrawBorder(worksheet, startRowNumber, startColumnNumber, rowNumber, columnNumber);

        }


        private static void DrawBlockTotal(StateBlock block, ExcelWorksheet worksheet, int rowNumber, int columnNumber)
        {

            int startRowNumber = rowNumber;
            int startColumnNumber = columnNumber;

            worksheet.Cells[rowNumber, columnNumber, rowNumber + 1, columnNumber + 2].Merge = true;
            worksheet.Cells[rowNumber, columnNumber + 3, rowNumber + 1, columnNumber + 6].Merge = true;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber + 7);

            columnNumber = 9;
            worksheet.Cells[rowNumber, columnNumber++].Value = block.LecturesBudget;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber++);
            worksheet.Cells[rowNumber, columnNumber++].Value = block.LabsBudget;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber++);
            worksheet.Cells[rowNumber, columnNumber++].Value = block.PracticeBudget;
            worksheet.Cells[rowNumber, columnNumber].Value = block.ModuleRatingSystemBudget;
            columnNumber += 2;
            worksheet.Cells[rowNumber, columnNumber++].Value = block.CourseProjectBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = block.ConsultationsBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = block.ReviewingTestsBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = block.OffsetsBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = block.ExamsBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = block.StudyPracticeBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = block.ProductionPracticeBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = block.ThesisProjectBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = block.StateExaminationComissionBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = "-";
            worksheet.Cells[rowNumber, columnNumber++].Value = block.TotalBudget;
            //
            rowNumber++;
            columnNumber = 9;
            worksheet.Cells[rowNumber, columnNumber].Value = block.LecturesOffBudget;
            columnNumber += 2;
            worksheet.Cells[rowNumber, columnNumber].Value = block.LabsOffBudget;
            columnNumber += 2;
            worksheet.Cells[rowNumber, columnNumber++].Value = block.PracticeOffBudget;
            worksheet.Cells[rowNumber, columnNumber].Value = block.ModuleRatingSystemOffBudget;
            columnNumber += 2;
            worksheet.Cells[rowNumber, columnNumber++].Value = block.CourseProjectOffBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = block.ConsultationsOffBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = block.ReviewingTestsOffBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = block.OffsetsOffBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = block.ExamsOffBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = block.StudyPracticeOffBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = block.ProductionPracticeOffBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = block.ThesisProjectOffBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = block.StateExaminationComissionOffBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = "-";
            worksheet.Cells[rowNumber, columnNumber].Value = block.TotalOffBudget;

            DrawBorder(worksheet, startRowNumber, startColumnNumber, rowNumber, columnNumber);
        }

        private static void DrawRow(StateRow row, ExcelWorksheet worksheet, int rowNumber, int columnNumber)
        {
            int startRowNumber = rowNumber;
            int startColumnNumber = columnNumber;

            MergeWithUnderneath(worksheet, rowNumber, columnNumber++);
            worksheet.Cells[rowNumber, columnNumber].Value = row.DisciplineName + "(" + row.SpecialityName + ")";
            MergeWithUnderneath(worksheet, rowNumber, columnNumber++);
            worksheet.Cells[rowNumber, columnNumber].Value = row.Course;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber++);
            worksheet.Cells[rowNumber, columnNumber++].Value = row.BudgetStudents;
            worksheet.Cells[rowNumber, columnNumber].Value = row.Streams;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber++);
            worksheet.Cells[rowNumber, columnNumber++].Value = row.BudgetGroups;
            worksheet.Cells[rowNumber, columnNumber++].Value = row.BudgetSubGroups;
            worksheet.Cells[rowNumber, columnNumber].Value = row.LecturesPlan;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber++);
            worksheet.Cells[rowNumber, columnNumber++].Value = (row.BudgetGroups >= row.OffBudgetGroups) ? row.Lectures.ToString() : "-";
            worksheet.Cells[rowNumber, columnNumber].Value = row.LabsPlan;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber++);
            worksheet.Cells[rowNumber, columnNumber++].Value = row.LabsBudget;
            worksheet.Cells[rowNumber, columnNumber].Value = row.PracticePlan;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber++);
            worksheet.Cells[rowNumber, columnNumber++].Value = row.PracticeBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = row.ModuleRatingSystemBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = "-";//Индивидуальные задания
            worksheet.Cells[rowNumber, columnNumber++].Value = row.CourseProjectBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = row.ConsultationsBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = row.ReviewingTestsBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = row.OffsetsBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = row.ExamsBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = row.EducationPracticeBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = row.InternshipBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = "-";
            worksheet.Cells[rowNumber, columnNumber++].Value = "-";
            worksheet.Cells[rowNumber, columnNumber++].Value = "-";
            worksheet.Cells[rowNumber, columnNumber++].Value = row.TotalBudget;
            /////
            columnNumber = 4;
            worksheet.Cells[(++rowNumber), columnNumber].Value = row.OffBudgetStudents;
            columnNumber += 2;
            worksheet.Cells[rowNumber, columnNumber++].Value = row.OffBudgetGroups;
            worksheet.Cells[rowNumber, columnNumber].Value = row.OffBudgetSubGroups;
            worksheet.Cells[rowNumber, columnNumber += 2].Value = (row.BudgetGroups < row.OffBudgetGroups) ? row.Lectures.ToString() : "-";
            worksheet.Cells[rowNumber, columnNumber += 2].Value = row.LabsOffBudget;
            worksheet.Cells[rowNumber, columnNumber+=2].Value = row.PracticeOffBudget;
            columnNumber++;
            worksheet.Cells[rowNumber, columnNumber++].Value = row.ModuleRatingSystemOffBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = "-";//Индивидуальные задания
            worksheet.Cells[rowNumber, columnNumber++].Value = row.CourseProjectOffBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = row.ConsultationsOffBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = row.ReviewingTestsOffBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = row.OffsetsOffBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = row.ExamsOffBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = row.EducationPracticeOffBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = row.InternshipOffBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = "-";
            worksheet.Cells[rowNumber, columnNumber++].Value = "-";
            worksheet.Cells[rowNumber, columnNumber++].Value = "-";
            worksheet.Cells[rowNumber, columnNumber].Value = row.TotalOffBudget;

            DrawBorder(worksheet, startRowNumber, startColumnNumber, rowNumber, columnNumber);
        }

        private static void DrawDiplomaRow(StateRow row, ExcelWorksheet worksheet, int rowNumber, int columnNumber)
        {
            int startRowNumber = rowNumber;
            int startColumnNumber = columnNumber;

            MergeWithUnderneath(worksheet, rowNumber, columnNumber++);
            worksheet.Cells[rowNumber, columnNumber].Value = row.DisciplineName + "(" + row.SpecialityName + ")";
            MergeWithUnderneath(worksheet, rowNumber, columnNumber++);
            worksheet.Cells[rowNumber, columnNumber].Value = row.Course;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber++);
            worksheet.Cells[rowNumber, columnNumber++].Value = row.BudgetStudents;
            worksheet.Cells[rowNumber, columnNumber].Value = row.Streams;
            MergeWithUnderneath(worksheet, rowNumber, columnNumber++);
            worksheet.Cells[rowNumber, columnNumber++].Value = row.BudgetGroups;
            worksheet.Cells[rowNumber, columnNumber++].Value = row.BudgetSubGroups;
            for (int i = columnNumber; i < 23; i++)
            {
                worksheet.Cells[rowNumber, columnNumber++].Value = "-";
            }
            worksheet.Cells[rowNumber, columnNumber++].Value = row.ThesisProjectBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = row.StateExaminationComissionBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = "-";
            worksheet.Cells[rowNumber, columnNumber++].Value = row.TotalBudget;
            /////
            columnNumber = 4;
            worksheet.Cells[(++rowNumber), columnNumber].Value = row.OffBudgetStudents;
            columnNumber += 2;
            worksheet.Cells[rowNumber, columnNumber++].Value = row.OffBudgetGroups;
            worksheet.Cells[rowNumber, columnNumber++].Value = row.OffBudgetSubGroups;
            for (int i = columnNumber; i < 23; i++)
            {
                worksheet.Cells[rowNumber, columnNumber++].Value = "-";
            }
            worksheet.Cells[rowNumber, columnNumber++].Value = row.ThesisProjectOffBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = row.StateExaminationComissionOffBudget;
            worksheet.Cells[rowNumber, columnNumber++].Value = "-";
            worksheet.Cells[rowNumber, columnNumber].Value = row.TotalOffBudget;

            DrawBorder(worksheet, startRowNumber, startColumnNumber, rowNumber, columnNumber);
        }



    }
}
