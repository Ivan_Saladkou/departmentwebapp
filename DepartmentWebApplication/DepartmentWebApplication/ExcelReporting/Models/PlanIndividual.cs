﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReporting.Models
{
    public class PlanIndividual
    {
        public double Lectures { get; set; }
        public double Labs { get; set; }
        public double Practices { get; set; }
        public double Sum { get { return Lectures + Labs + Practices; } }


        public PlanIndividual() { }

        public PlanIndividual(double lectures, double labs, double practices)
        {
            Lectures = lectures;
            Labs = labs;
            Practices = practices;
        }
    }
}
