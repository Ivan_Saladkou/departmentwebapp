﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReporting.Models
{
    public class PlanIndividualEmployees
    {
        public EmployeeIndividual Lecture { get; set; }
        public EmployeeIndividual Labs { get; set; }
        public EmployeeIndividual Practice { get; set; }
    }
}
