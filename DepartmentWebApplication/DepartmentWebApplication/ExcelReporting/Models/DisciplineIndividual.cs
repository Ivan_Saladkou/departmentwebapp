﻿using ExcelReporting.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReporting.Models
{
    public class DisciplineIndividual
    {
        public PlanIndividualEmployees[] BudgetPeoples { get; set; }
        public PlanIndividualEmployees[] FeePeoples { get; set; }

        public int BudgetCount { get; set; }
        public int FeeCount { get; set; }

        public string Name { get; set; }
        public int Course { get; set; }

        public Semester? Semester { get; set; }
        public Stage? Stage { get; set; }
        public Department? Department { get; set; }

        public PlanIndividual Plan { get; set; }
        public PlanIndividual ForPeople { get; set; }

        public double Sum { get { return Plan.Sum; } }
        public List<double> Hours { get; set; } = new List<double>();

        public override string ToString()
        {
            return Name;
        }

        public DisciplineIndividual() { }


        public static bool operator ==(DisciplineIndividual left, DisciplineIndividual right)
        {
            return (left.Course == right.Course && left.Name == right.Name && left.Semester == right.Semester);
        }

        public static bool operator !=(DisciplineIndividual left, DisciplineIndividual right)
        {
            return !(left == right);
        }
    }
}
