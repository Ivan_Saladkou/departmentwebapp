﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReporting.Models
{
    public class EmployeeIndividual
    {
        public string Name { get; set; }
        public string Position { get; set; }
        public List<DisciplineIndividual> Disciplines { get; set; } = new List<DisciplineIndividual>();

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return ((obj as EmployeeIndividual).Name == this.Name && (obj as EmployeeIndividual).Position == this.Position);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() + Position.GetHashCode();
        }

        public override string ToString()
        {
            return Name;
        }

        public EmployeeIndividual() { }
    }
}
