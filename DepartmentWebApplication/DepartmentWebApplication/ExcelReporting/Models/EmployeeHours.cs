﻿using ExcelReporting.Enums;
using System;
using System.Collections.Generic;

namespace ExcelReporting.Models
{
    public class EmployeeHours
    {
        private Dictionary<string, double[]> hoursDoneByMonth = new Dictionary<string, double[]>();
        private double[][] hoursPlanned = new double[2][];
        public string Name { get; }

        public EmployeeHours(string name)
        {
            Name = name;
        }

        public void AddHoursDone(string month, double[] hours)
        {
            if (!hoursDoneByMonth.ContainsKey(month))
            {
                hoursDoneByMonth.Add(month, hours);
            }
        }

        public double[] GetHoursDone(string month)
        {
            return hoursDoneByMonth[month];
        }

        public void SetHoursPlanned(int index, double[] hours)
        {
            hoursPlanned[index] = hours;
        }

        public double[] GetHoursPlanned(StudyPeriod period)
        {
            switch (period)
            {
                case StudyPeriod.FirstHalf:
                    return hoursPlanned[0];
                case StudyPeriod.SecondHalf:
                    return hoursPlanned[1];
                case StudyPeriod.Year:
                    double[] result = new double[hoursPlanned[0].Length];
                    for (int i = 0; i < result.Length; i++)
                    {
                        result[i] = hoursPlanned[0][i] + hoursPlanned[1][i];
                    }
                    return result;
                default:
                    throw new ArgumentException();
            }
        }
    }
}
