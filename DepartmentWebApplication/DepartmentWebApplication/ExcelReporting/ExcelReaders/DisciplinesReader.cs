﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExcelReporting.Enums;
using ExcelReporting.Models;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using OfficeOpenXml;

namespace ExcelReporting.ExcelReaders
{
    public class DisciplinesReader
    {
        public List<DisciplineIndividual> Get(string path, string sheetName)
        {
            List<DisciplineIndividual> disciplines = new List<DisciplineIndividual>();

            using (var excel = new ExcelPackage(new FileInfo(path)))
            {
                var worksheet = excel.Workbook.Worksheets[sheetName];
                //                worksheet.Calculate();

                List<ExcelRange> cells = Group(worksheet);
                IEnumerable<ExcelRange> stages = cells
                    .Where(e => e.Value.ToString().ToLower().Contains("студенты"))
                    .OrderBy(e => -e.Start.Row)
                    .ToList();
                IEnumerable<ExcelRange> semesters = cells
                    .Where(e => e.Value.ToString().ToLower().Contains("семестр"))
                    .OrderBy(e => -e.Start.Row)
                    .ToList();

                DisciplineRowIds(worksheet).ForEach(e => disciplines.Add(GetDiscipline(e, worksheet, stages, semesters)));
            }
            return disciplines;
        }

        public static void FillWithPeoples(IEnumerable<DisciplineIndividual> disciplines, IEnumerable<EmployeeIndividual> peoples)
        {
            List<EmployeeIndividual> peoplesForDiscipline;
            Dictionary<EmployeeIndividual, DisciplineIndividual> Dictionary;
            foreach (DisciplineIndividual discipline in disciplines)
            {
                peoplesForDiscipline = new List<EmployeeIndividual>();
                Dictionary = new Dictionary<EmployeeIndividual, DisciplineIndividual>();
                foreach (EmployeeIndividual people in peoples)
                    foreach (DisciplineIndividual disciplineOfPeople in people.Disciplines)
                        if (discipline == disciplineOfPeople)
                        {
                            peoplesForDiscipline.Add(people);
                            Dictionary.Add(people, disciplineOfPeople);
                        }

                for (int i = 0; i < discipline.BudgetCount; i++)
                {
                    discipline.BudgetPeoples[i].Lecture = peoplesForDiscipline.Find(e => discipline.Plan.Lectures != 0 && Dictionary[e].ForPeople.Lectures >= discipline.Plan.Lectures);
                }
                for (int i = 0; i < discipline.BudgetCount; i++)
                {
                    if (discipline.Plan.Labs != 0)
                    {
                        discipline.BudgetPeoples[i].Labs = peoplesForDiscipline.Find(e => discipline.Plan.Labs != 0 && Dictionary[e].ForPeople.Labs >= discipline.Plan.Labs);
                        if (discipline.BudgetPeoples[i].Labs != null)
                            Dictionary[discipline.BudgetPeoples[i].Labs].ForPeople.Labs -= discipline.Plan.Labs;
                    }
                    if (discipline.Plan.Practices != 0)
                    {
                        discipline.BudgetPeoples[i].Practice = peoplesForDiscipline.Find(e => discipline.Plan.Practices != 0 && Dictionary[e].ForPeople.Practices >= discipline.Plan.Practices);
                        if (discipline.BudgetPeoples[i].Practice != null)
                            Dictionary[discipline.BudgetPeoples[i].Practice].ForPeople.Practices -= discipline.Plan.Practices;
                    }
                }

                for (int i = 0; i < discipline.FeeCount; i++)
                {
                    if (discipline.Plan.Labs != 0)
                    {
                        discipline.FeePeoples[i].Labs = peoplesForDiscipline.Find(e => discipline.Plan.Labs != 0 && Dictionary[e].ForPeople.Labs >= discipline.Plan.Labs);
                        if (discipline.FeePeoples[i].Labs != null)
                            Dictionary[discipline.FeePeoples[i].Labs].ForPeople.Labs -= discipline.Plan.Labs;
                    }
                    if (discipline.Plan.Practices != 0)
                    {
                        discipline.FeePeoples[i].Practice = peoplesForDiscipline.Find(e => discipline.Plan.Practices != 0 && Dictionary[e].ForPeople.Practices >= discipline.Plan.Practices);
                        if (discipline.FeePeoples[i].Practice != null)
                            Dictionary[discipline.FeePeoples[i].Practice].ForPeople.Practices -= discipline.Plan.Practices;
                    }
                }
            }
        }

        private DisciplineIndividual GetDiscipline(int rowId, ExcelWorksheet worksheet, IEnumerable<ExcelRange> stages, IEnumerable<ExcelRange> semesters)
        {
            Stage stage = Stage.First;
            Department department = Department.Extramural;
            Semester semester = Semester.Spring;
            foreach (ExcelRange stageCell in stages)
            {
                if (rowId > stageCell.Start.Row)
                {
                    stage = stageCell.Value.ToString().ToLower().Contains("ii") ? Stage.Second : Stage.First;
                    break;
                }
            }
            foreach (ExcelRange semesterCell in semesters)
            {
                if (rowId > semesterCell.Start.Row)
                {
                    semester = semesterCell.Value.ToString().ToLower().Contains("осенний") ? Semester.Autumn : Semester.Spring;
                    department = semesterCell.Value.ToString().ToLower().Contains("дневное") ? Department.Daytime : Department.Extramural;
                    break;
                }
            }

            int BudgetCount = Convert.ToInt32(MyConverter.Convert(worksheet.Cells[rowId, 6].Value) * MyConverter.Convert(worksheet.Cells[rowId, 7].Value));
            int FeeCount = Convert.ToInt32(MyConverter.Convert(worksheet.Cells[rowId + 1, 6].Value) * MyConverter.Convert(worksheet.Cells[rowId + 1, 7].Value));
            PlanIndividualEmployees[] BudgetPeoples = new PlanIndividualEmployees[BudgetCount];
            for (int i = 0; i < BudgetCount; i++)
                BudgetPeoples[i] = new PlanIndividualEmployees();
            PlanIndividualEmployees[] FeePeoples = new PlanIndividualEmployees[FeeCount];
            for (int i = 0; i < FeeCount; i++)
                FeePeoples[i] = new PlanIndividualEmployees();

            return new DisciplineIndividual
            {
                BudgetPeoples = BudgetPeoples,
                FeePeoples = FeePeoples,
                BudgetCount = BudgetCount,
                FeeCount = FeeCount,
                Name = worksheet.Cells[rowId, 2].Value.ToString(),
                Course = Convert.ToInt32(MyConverter.Convert(worksheet.Cells[rowId, 3].Value)),
                Semester = semester,
                Department = department,
                Stage = stage,
                Plan = new PlanIndividual
                {
                    Lectures = MyConverter.Convert(worksheet.Cells[rowId, 8].Value),
                    Labs = MyConverter.Convert(worksheet.Cells[rowId, 10].Value),
                    Practices = MyConverter.Convert(worksheet.Cells[rowId, 12].Value)
                }
            };
        }
        private List<int> DisciplineRowIds(ExcelWorksheet worksheet)
        {
            char columnName = 'B';
            List<int> indexes = new List<int>();
            var merged = worksheet.MergedCells
                .Where(e => e.Contains(columnName))
                .OrderBy(e => e.Length)
                .ThenBy(e => e);
            string[] cellNames;
            foreach (var rangeName in merged)
            {
                cellNames = rangeName.Split(':');
                if (cellNames[1].Contains(columnName) && cellNames[0].Contains(columnName))
                {
                    var range = worksheet.Cells[rangeName];
                    int mergedRowsCount = range.Rows;
                    var firstCell = worksheet.Cells[cellNames[0]];
                    string cellId = "A" + firstCell.Start.Row;

                    if (mergedRowsCount == 2 && firstCell.Value != null && worksheet.Cells[cellId].Value != null)
                    {
                        indexes.Add(firstCell.Start.Row);
                    }
                }
            }
            return indexes;
        }
        private List<ExcelRange> Group(ExcelWorksheet worksheet)
        {
            List<ExcelRange> indexes = new List<ExcelRange>();

            var merged = worksheet.MergedCells
                .Where(e => e.Contains("F"))
                .OrderBy(e => e.Length)
                .ThenBy(e => e);
            string[] cellNames;

            foreach (var rangeName in merged)
            {
                cellNames = rangeName.Split(':');
                if (cellNames[1].Contains("N"))
                {
                    var firstCell = worksheet.Cells[cellNames[0]];

                    if (firstCell.Value != null)
                    {
                        indexes.Add(firstCell);
                    }
                }
            }
            return indexes;
        }//4 13
    }
}
