﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using ExcelReporting.Models;
using System.IO;
using ExcelReporting.Enums;

namespace ExcelReporting.ExcelReaders
{
    public class DistributionReader
    {
        public List<EmployeeIndividual> Get(string path, string sheetName)
        {
            List<EmployeeIndividual> peoples = new List<EmployeeIndividual>();

            using (var excel = new ExcelPackage(new FileInfo(path)))
            {
                ExcelWorksheet worksheet = excel.Workbook.Worksheets[sheetName];
                List<int> indexes = EmployeeRowIds(worksheet);
                indexes = indexes.ToList();
                indexes.ForEach(e => peoples.Add(Fill(e, worksheet)));
            }
            return peoples.Where(e => e.Disciplines.Count != 0).ToList();
        }

        private EmployeeIndividual Fill(int rowId, ExcelWorksheet worksheet)
        {
            Semester semester = Semester.Autumn;
            int rowIdStart = rowId + 2;
            EmployeeIndividual people = new EmployeeIndividual()
            {
                Name = worksheet.Cells[rowIdStart - 2, 2].Value.ToString(),
                Position = worksheet.Cells[rowIdStart - 2, 3].Value?.ToString() ?? ""
            };

            if (people.Position != "" && people.Position.Last() == '.')
                people.Position = people.Position.Substring(0, people.Position.Length - 1);

            DisciplineIndividual temp;
            List<DisciplineIndividual> disciplines = new List<DisciplineIndividual>();

            while ((!worksheet.Cells[rowIdStart, 2].Merge && rowIdStart <= 3000) || (worksheet.Cells[rowIdStart, 2].Value?.ToString() == "Весна"))
            {
                if (worksheet.Cells[rowIdStart, 2].Value?.ToString() == "Весна")
                {
                    semester = Semester.Spring;
                    rowIdStart++;
                    continue;
                }
                else if (worksheet.Cells[rowIdStart, 2].Value == null)
                {
                    rowIdStart++;
                    continue;
                }
                temp = new DisciplineIndividual() { Name = worksheet.Cells[rowIdStart, 2].Value.ToString() };
                temp.Semester = semester;
                temp.Course = Convert.ToInt32(MyConverter.Convert(worksheet.Cells[rowIdStart, 3].Value));
                for (int column = 7; column < 22; column++)
                {
                    temp.Hours.Add(MyConverter.Convert(worksheet.Cells[rowIdStart, column].Value));
                }
                temp.ForPeople = new PlanIndividual(MyConverter.Convert(worksheet.Cells[rowIdStart, 7].Value), MyConverter.Convert(worksheet.Cells[rowIdStart, 8].Value),
                                        MyConverter.Convert(worksheet.Cells[rowIdStart, 9].Value));
                disciplines.Add(temp);
                rowIdStart++;
            }
            people.Disciplines = disciplines;
            return people;
        }

        private List<int> EmployeeRowIds(ExcelWorksheet worksheet)
        {
            char columnName = 'B';
            List<int> indexes = new List<int>();
            var merged = worksheet.MergedCells
                .Where(e => e.Contains(columnName))
                .OrderBy(e => e.Length)
                .ThenBy(e => e);
            string[] cellNames;
            foreach (var rangeName in merged)
            {
                cellNames = rangeName.Split(':');
                if (cellNames[1].Contains(columnName) && cellNames[0].Contains(columnName))
                {
                    var range = worksheet.Cells[rangeName];
                    int mergedRowsCount = range.Rows;
                    var firstCell = worksheet.Cells[cellNames[0]];
                    string cellId = "A" + firstCell.Start.Row;

                    if (mergedRowsCount == 2 && firstCell.Value != null && worksheet.Cells[cellId].Value != null)
                    {
                        indexes.Add(firstCell.Start.Row);
                    }
                }
            }
            return indexes;
        }
    }
}
