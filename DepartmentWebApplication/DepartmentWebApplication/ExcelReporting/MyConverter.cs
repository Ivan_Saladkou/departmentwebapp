﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReporting
{
    public static class MyConverter
    {
        public static double Around(double value, double denominator)
        {
            value /= denominator;
            double f = value % 2;
            if (f > 1)
                f--;
            if (f > 0.75)
                value += 1;
            else if (f >= 0.25)
                value += 0.5;
            return value - f;
        }

        public static double Convert(object hours)
        {
            if (hours == null)
                return 0;
            return Convert(hours.ToString());
        }

        //раньше toDouble возвращал "0,5" => 0.5 . Сейчас 5 
        public static double Convert(string hours)
        {
            return hours == "" ? 0 : System.Convert.ToDouble(hours.Replace(".", ",")); //ToDouble "0,5" => 5, "0.5" => 5
        }
    }
}
