﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReporting.Enums
{
    public enum StudyPeriod
    {
        [Display(Name = "I-е полугодие")]
        FirstHalf= 1,
        [Display(Name = "II-е полугодие")]
        SecondHalf = 2,
        [Display(Name = "год")]
        Year = 3
    }
}
