﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReporting.Enums
{
    public enum StudyMonth
    {
        [Display(Name = "Сентябрь")]
        September = 1,
        [Display(Name = "Октябрь")]
        October = 2,
        [Display(Name = "Ноябрь")]
        November = 3,
        [Display(Name = "Декабрь")]
        December = 4,
        [Display(Name = "Январь")]
        January = 5,

        [Display(Name = "Февраль")]
        February = 6,
        [Display(Name = "Март")]
        March = 7,
        [Display(Name = "Апрель")]
        April = 8,
        [Display(Name = "Май")]
        May = 9,
        [Display(Name = "Июнь")]
        June = 10,
    }
}
