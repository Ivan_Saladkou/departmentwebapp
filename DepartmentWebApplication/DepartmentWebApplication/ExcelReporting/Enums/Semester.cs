﻿using System.ComponentModel.DataAnnotations;

namespace ExcelReporting.Enums
{
    public enum Semester
    {
        [Display(Name = "Осень")]
        Autumn,
        [Display(Name = "Весна")]
        Spring
    }
}
