﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Enums;
using DataAccessLayer.Entities;
using DataAccessLayer.Extensions;

namespace DataAccessLayer.Repositories
{
    public class SettingRepository : Repository
    {
        public void Edit(SettingEnum setting, string value)
        {
            using (db = new DepartmentEntities())
            {
                var settingName = setting.GetDisplayName();
                var oldSetting = db.Settings.First(e => e.Name == settingName);
                oldSetting.Value = value;
                db.SaveChanges();
            }
        }

        public string GetValue(SettingEnum setting)
        {
            using (db = new DepartmentEntities())
            {
                var settingName = setting.GetDisplayName();
                return db.Settings.FirstOrDefault(e => e.Name == settingName)?.Value;
            }
        }

        public List<Setting> GetAll()
        {
            using (db = new DepartmentEntities())
            {
                return db.Settings.ToList();
            }
        }
    }
}
