﻿using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System;

namespace DataAccessLayer.Repositories
{
    public class SpecialityRepository : Repository, ISpecialityRepository
    {
        public void Add(Speciality item)
        {
            throw new NotImplementedException();
        }

        public int AddGetId(Speciality speciality)
        {
            int specialityId;
            using(db = new DepartmentEntities())
            {
                db.Specialities.Add(speciality);
                db.SaveChanges();
                specialityId = speciality.Id;
            }
            return specialityId;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Edit(int id, Speciality item)
        {
            throw new NotImplementedException();
        }

        public List<Speciality> GetAll()
        {
            using(db = new DepartmentEntities())
            {
                return db.Specialities.Include(e=> e.Contingents).Include(e=> e.Contingents.Select(ec=> ec.Courses)).ToList();
            }
        }

        public Speciality GetItem(int id)
        {
            throw new NotImplementedException();
        }
    }
}
