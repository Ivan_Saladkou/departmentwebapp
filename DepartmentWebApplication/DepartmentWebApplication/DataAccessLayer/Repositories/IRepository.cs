﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        List<TEntity> GetAll();

        TEntity GetItem(int id);

        void Add(TEntity item);

        void Edit(int id, TEntity item);

        void Delete(int id);
    }
}
