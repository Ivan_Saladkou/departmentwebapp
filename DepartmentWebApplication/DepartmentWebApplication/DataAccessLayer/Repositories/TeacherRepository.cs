﻿using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public class TeacherRepository : Repository, ITeacherRepository
    {
        public void Add(Teacher item)
        {
            using (db = new DepartmentEntities())
            {
                db.Teachers.Add(item);
                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Edit(int id, Teacher item)
        {
            throw new NotImplementedException();
        }

        public List<Teacher> GetAll()
        {
            using (db = new DepartmentEntities())
            {
                return db.Teachers.ToList();
            }
        }

        public Teacher GetItem(int id)
        {
            throw new NotImplementedException();
        }
    }
}
