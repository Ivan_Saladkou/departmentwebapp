﻿using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public class DisciplineRepository : Repository, IDisciplineRepository
    {
        public void Add(Subject item)
        {
            using (db = new DepartmentEntities())
            {
                db.Subjects.Add(item);
                db.SaveChanges();
            }
        }

        public int AddGetId(Subject discipline)
        {
            int disciplineId;
            using (db = new DepartmentEntities())
            {
                db.Subjects.Add(discipline);
                db.SaveChanges();
                disciplineId = discipline.Id;
            }
            return disciplineId;
        }

        public void Delete(int id)
        {
            using (db = new DepartmentEntities())
            {
                Subject removable = db.Subjects.Find(id);
                db.Subjects.Remove(removable);
                db.SaveChanges();
            }
        }

        public void Edit(int id, Subject item)
        {
            //using (db = new DepartmentEntities())
            //{
            //    Discipline changable = db.Disciplines.Find(id);

            //    changable.Id = item.Id;
            //    changable.PlanId = item.PlanId;

            //    changable.SequentialNumber = item.SequentialNumber;
            //    changable.Name = item.Name;
            //    changable.NameAbbreviated = item.NameAbbreviated;

            //    //changable.Control = item.Control;
            //    //changable.CourseType = item.CourseType;
            //    //changable.Lectures = item.Lectures;
            //    //changable.Labs = item.Labs;
            //    //changable.Practices = item.Practices;
            //    //changable.Seminars = item.Seminars;
            //    //changable.Independents = item.Independents;
            //    //changable.Ksr = item.Ksr;

            //    db.SaveChanges();
        }

        public List<Subject> GetAll()
        {
            throw new NotImplementedException();
        }

        public Subject GetItem(int id)
        {
            throw new NotImplementedException();
        }
    }

    //public List<Discipline> GetAll()
    //{
       
    //    //using (db = new DepartmentEntities())
    //    //{
    //    //    return db.Disciplines.Include(d => d.DisciplineSemesters).ToList();
    //    //}
    //}

    //public Discipline GetItem(int id)
    //{
       
    //    //using (db = new DepartmentEntities())
    //    //{
    //    //    return db.Disciplines.Find(id);
    //    //}
    //}
}

