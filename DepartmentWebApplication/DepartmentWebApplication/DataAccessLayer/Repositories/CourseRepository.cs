﻿using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public class CourseRepository : Repository, ICourseRepository
    {
        public void Add(Course item)
        {
            using (db = new DepartmentEntities())
            {
                db.Courses.Add(item);
                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Edit(int id, Course item)
        {
            using (db = new DepartmentEntities())
            {
                Course changable = db.Courses.Find(id);
                changable.Number = item.Number;
                changable.Budget = item.Budget;
                changable.OffBudget = item.OffBudget;
                changable.Foreign = item.Foreign;
                changable.BudgetGroups = item.BudgetGroups;
                changable.OffBudgetGroups = item.OffBudgetGroups;
                changable.ContingentId = item.ContingentId;

                db.SaveChanges();
            }
        }

        public List<Course> GetAll()
        {
            using (db = new DepartmentEntities())
            {
                return db.Courses.ToList();
            }
        }

        public Course GetItem(int id)
        {
            throw new NotImplementedException();
        }
    }
}
