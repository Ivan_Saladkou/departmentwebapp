﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;

namespace DataAccessLayer.Repositories
{
    public class DisciplineSemesterRepository : Repository, IDisciplineSemester
    {
        public void Add(Discipline item)
        {
            using (db = new DepartmentEntities())
            {
                db.Disciplines.Add(item);
                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Edit(int id, Discipline item)
        {
            throw new NotImplementedException();
        }

        public List<Discipline> GetAll()
        {
            using (db = new DepartmentEntities())
            {
                return db.Disciplines.ToList();
            }
        }

        public Discipline GetItem(int id)
        {
            throw new NotImplementedException();
        }
    }
}
