﻿using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace DataAccessLayer.Repositories
{
    public class PlanRepository : Repository, IPlanRepository
    {
        public void Add(Plan item)
        {
            using (db = new DepartmentEntities())
            {
                db.Plans.Add(item);
                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (db = new DepartmentEntities())
            {
                Plan removable = db.Plans.Find(id);
                db.Plans.Remove(removable);
                db.SaveChanges();
            }
        }

        public void Edit(int id, Plan item)
        {
            using (db = new DepartmentEntities())
            {
                Plan changable = db.Plans.Find(id);
                changable.SpecialityId = item.SpecialityId;
                changable.Specialization = item.Specialization;
                changable.Duration = item.Duration;
                changable.Date = item.Date;
                changable.RegNumber = item.RegNumber;
                changable.Stage = item.Stage;
                changable.FormStudy = item.FormStudy;
                changable.Budget = item.Budget;

                db.SaveChanges();
            }
        }

        public List<Plan> GetAll()
        {
            using (db = new DepartmentEntities())
            {
                return db.Plans
                    .Include(e => e.Speciality)
                    .Include(e => e.Disciplines)
                    .Include(e => e.Semesters)
                    .ToList();
            }
        }

        public Plan GetItem(int id)
        {
            using (db = new DepartmentEntities())
            {
                return db.Plans
                    .Include(e => e.Disciplines.Select(d => d.Subject))
                    .Include(e => e.Semesters)
                    .Include(e => e.Speciality)
                    .Include(e => e.Disciplines)
                    .Include(e => e.Practices)
                    .Where(e => e.Id == id).First();
            }
        }
    }
}
