﻿using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System;

namespace DataAccessLayer.Repositories
{
    public class ContingentRepository : Repository, IContingentRepository
    {
        public void Add(Contingent item)
        {
            using (db = new DepartmentEntities())
            {
                db.Contingents.Add(item);
                db.SaveChanges(); //добавит и курсы которые есть в contingent
            }
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Edit(int id, Contingent item)
        {
            throw new NotImplementedException();
        }

        public List<Contingent> GetAll()
        {
            using (db = new DepartmentEntities())
            {
                return db.Contingents.Include(e => e.Speciality).ToList();
            }
        }

        public Contingent GetItem(int id)
        {
            throw new NotImplementedException();
        }
    }
}
