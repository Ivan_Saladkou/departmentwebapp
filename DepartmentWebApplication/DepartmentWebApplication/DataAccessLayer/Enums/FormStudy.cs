﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum FormStudy
    {
        [Display(Name = "Очное")]
        FullTime,
        [Display(Name = "Заочное")]
        Extramural
    }
}
