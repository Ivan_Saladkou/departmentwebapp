﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{

    //ГЭК = Государственная экзаменационная комиссия = State Examenation Commission = SEC
    //Undergraduate = магистрант
    public enum DistributionWorkType
    {
        Lectures,
        Labs,
        Practice,
        ModuleRatingSystem,
        IndividualsTasks,
        CourseProject,
        Consultations,
        ReviewingTests,
        Offsets,
        Exams,
        EducationPractice,
        Internship,
        ThesisProject,
        StateExaminationComission
    }
}