﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum SettingEnum
    {
        [Display(Name="Путь отчета по индивидуальным планам")]
        ReportIndividualPlansPath,
        [Display(Name = "Путь индивидуальных планов")]
        IndividualPlansPath,
        [Display(Name = "Путь сведений к расписанию")]
        DisciplinesInformationsPath,
        [Display(Name = "Путь распределения РБ")]
        DistributionsRBPath,
        [Display(Name = "Путь распределения РФ")]
        DistributionsRFPath,
        [Display(Name = "Путь по семестрам РФ")]
        DisciplinesBySemesterRFPath,
        [Display(Name = "Путь по семестрам РБ")]
        DisciplinesBySemesterRBPath,
        [Display(Name = "Путь шаблона индивидуального плана")]
        IndividualPlanTempPath,
        [Display(Name = "Путь шаблона сведения к расписанию")]
        DisciplinesInformationTempPath,
    }
}
