﻿using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.Enums
{
    public enum Stage
    {
        [Display(Name = "Бакалавриат")]
        First,
        [Display(Name = "Магистратура")]
        Second
    }
}