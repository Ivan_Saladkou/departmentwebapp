﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum Budget
    {
        [Display(Name = "Рф")]
        Rf,
        [Display(Name = "Рб")]
        Rb
    }
}
