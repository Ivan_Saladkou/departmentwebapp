﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum TimeOfYear
    {
        [Display(Name = "Весна")]
        Spring,
        [Display(Name = "Осень")]
        Autumn
    }
}
