﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{

    //ГЭК = Государственная экзаменационная комиссия = State Examenation Commission = SEC
    //Undergraduate = магистрант
    public enum WorkType
    {
        [Display(Name = "Модульно-рейтинговая система")]
        ModuleRatingSystem,
        [Display(Name = "Курсовая работа")]
        CourseWork,
        [Display(Name = "Курсовой проект")]
        CourseProject,
        [Display(Name = "Проверка тестов")]
        ReviewingTests,
        [Display(Name = "Зачет")]
        Offset,
        [Display(Name = "Экзамен")]
        Exam,
        [Display(Name = "Консультация")]
        Consultation,
        [Display(Name = "Рейтинг контроль")]
        RatingControl,
        [Display(Name = "Дипломный проект РФ")]
        ThesisProjectRussian,
        [Display(Name = "Дипломный проект РБ")]
        ThesisProjectBelorussian,
        [Display(Name = "Председатель ГЭК")]
        SecChairman,
        [Display(Name = "Член ГЭК")]
        SecMember,
        [Display(Name = "Нормоконтроль")]
        SecNormControl,
        [Display(Name = "Утверждение")]
        SecApproval,
        [Display(Name = "Рецензирование")]
        SecReview,
        [Display(Name = "Количество членов ГЭК")]
        SecMemberAmount,//перенести в более подходящее место
        [Display(Name = "Дипломный руководитель")]
        GuidanceOfUndergraduates,
        [Display(Name = "Председатель")]
        UndergraduateChairman,
        [Display(Name = "Член комисии")]
        UndergraduateMember,
        [Display(Name = "Нормоконтроль")]
        UndergraduateNormControl,
        [Display(Name = "Утверждение")]
        UndergraduateApproval,
        [Display(Name = "Рецензирование")]
        UndergraduateReview,
        [Display(Name = "Количество членов в комиссии")]
        UndergraduateMemberAmount,//перенести в более подходящее место
        [Display(Name = "Учебная практика")]
        EducationalPractice,
        [Display(Name = "Производственная практика")]
        Internship        
    }
}
