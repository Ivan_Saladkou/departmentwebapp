﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.Enums
{
    public enum Position
    {
        [Display(Name = "Ассистент")]
        Assistant,
        [Display(Name = "Преподаватель")]
        Teacher,
        [Display(Name = "Старший преподаватель")]
        SeniorTeacher,
        [Display(Name = "Доцент")]
        Docent,
        [Display(Name = "Профессор")]
        Professor
    }
}
