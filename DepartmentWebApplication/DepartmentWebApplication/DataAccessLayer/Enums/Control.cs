﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum Control
    {
        [Display(Name = "Экзамен")]
        Exam,
        [Display(Name = "Зачет")]
        Credit
    }
}
