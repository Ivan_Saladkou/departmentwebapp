﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities
{
    public class StateRow
    {
        public int Id { get; set; }
        public int StateBlockId { get; set; }
        public StateBlock StateBlock { get; set; }
        public string DisciplineName { get; set; }
        public string SpecialityName { get; set; }
        public int Course { get; set; }
        public int BudgetStudents { get; set; }
        public int OffBudgetStudents { get; set; }
        public double Streams { get; set; }
        public double BudgetGroups { get; set; }
        public double OffBudgetGroups { get; set; }
        public double BudgetSubGroups { get; set; }
        public double OffBudgetSubGroups { get; set; }
        public double Lectures { get; set; }
        public double LecturesPlan { get; set; }
        public double LabsBudget { get; set; }
        public double LabsOffBudget { get; set; }
        public double LabsPlan { get; set; }
        public double PracticeBudget { get; set; }
        public double PracticeOffBudget { get; set; }
        public double PracticePlan { get; set; }
        public double ModuleRatingSystemBudget { get; set; }
        public double ModuleRatingSystemOffBudget { get; set; }
        public double IndividualsTasks { get; set; }//пока не берется в расчет
        public double CourseProjectBudget { get; set; }
        public double CourseProjectOffBudget { get; set; }
        public double ConsultationsBudget { get; set; }
        public double ConsultationsOffBudget { get; set; }
        public double ReviewingTestsBudget { get; set; }//Рецензирование контрольных работ
        public double ReviewingTestsOffBudget { get; set; }
        public double OffsetsBudget { get; set; }
        public double OffsetsOffBudget { get; set; }
        public double ExamsBudget { get; set; }
        public double ExamsOffBudget { get; set; }
        public double EducationPracticeBudget { get; set; }
        public double EducationPracticeOffBudget { get; set; }
        public double InternshipBudget { get; set; }
        public double InternshipOffBudget { get; set; }
        public double ThesisProjectBudget { get; set; }//Дипломное проектирование
        public double ThesisProjectOffBudget { get; set; }
        public double StateExaminationComissionBudget { get; set; }//ГЭК
        public double StateExaminationComissionOffBudget { get; set; }
        public double PostgraduateStudies { get; set; }//Аспирант.
     

        //Как считаются лекции?
        //Аспирант. не учитывается
        //ГЭК не учитывается
        [NotMapped]
        public double TotalBudget
        {
            get
            {
                return Lectures + LabsBudget + PracticeBudget + ModuleRatingSystemBudget +
                    CourseProjectBudget + ConsultationsBudget + ReviewingTestsBudget + OffsetsBudget
                    + ExamsBudget + EducationPracticeBudget + InternshipBudget + ThesisProjectBudget;
            }

        }

        [NotMapped]
        public double TotalOffBudget
        {
            get
            {
                return Lectures + LabsOffBudget + PracticeOffBudget + ModuleRatingSystemOffBudget +
                    CourseProjectOffBudget + ConsultationsOffBudget + ReviewingTestsOffBudget + OffsetsOffBudget
                    + ExamsOffBudget + EducationPracticeOffBudget + InternshipOffBudget + ThesisProjectOffBudget;
            }

        }

    }
}
