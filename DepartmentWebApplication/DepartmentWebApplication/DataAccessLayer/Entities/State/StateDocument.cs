﻿using DataAccessLayer.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.Entities
{
    public class StateDocument
    {
        public int Id { get; set; }

        [DataType(DataType.Date, ErrorMessage = "Date only")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        public DateTime FormationDate { get; set; }
        public int DepartmentId { get; set; }
        public Department Department { get; set; }
        public Budget Budget { get; set; }
        public ICollection<StateBlock> StateBlocks { get; set; }
    }
}
