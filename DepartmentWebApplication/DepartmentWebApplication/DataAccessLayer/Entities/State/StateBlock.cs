﻿using DataAccessLayer.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities
{
    public class StateBlock
    {
        public int Id { get; set; }

        public Stage Stage { get; set; }
        public FormStudy FormStudy { get; set; }
        public TimeOfYear TimeOfYear { get; set; }
        public int StateDocumentId { get; set; }
        public StateDocument StateDocument { get; set; }
        public ICollection<StateRow> StateRows { get; set; }
        //public ICollection<StateRow> StateExaminationComissionDisciplines { get; set; }

        [NotMapped]
        private List<string> SecDisciplines = new List<string> {
            "Преддипломная практика",
            "Дипломное проектирование",
            "Председатель ГЭК",
            "Члены ГЭК",
            "Нормаконтроль",
            "Утверждение",
            "Рецензирование"
        };

        [NotMapped]
        public List<StateRow> StateExaminationComissionDisciplines
        {

            get
            {
                return StateRows.Where(row => SecDisciplines.Contains(row.DisciplineName)).ToList();

            }

        }

        [NotMapped]
        public double Lectures
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.Lectures;
                }
                return sum;

            }

        }

        [NotMapped]
        public double LecturesBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    if (row.BudgetGroups >= row.OffBudgetGroups) {
                        sum += row.Lectures;
                    }                   
                }
                return sum;

            }
        }

        [NotMapped]
        public double LecturesOffBudget
        {

            get
            {
                return Lectures - LecturesBudget;           
            }
        }

        [NotMapped]
        public double LabsBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.LabsBudget;
                }
                return sum;

            }

        }

        [NotMapped]
        public double LabsOffBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.LabsOffBudget;
                }
                return sum;

            }

        }

        [NotMapped]
        public double LabsTotal
        {

            get
            {
                return LabsBudget + LabsOffBudget;

            }

        }

        [NotMapped]
        public double PracticeBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.PracticeBudget;
                }
                return sum;

            }

        }

        [NotMapped]
        public double PracticeOffBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.PracticeOffBudget;
                }
                return sum;

            }

        }

        [NotMapped]
        public double PracticeTotal
        {

            get
            {
                return PracticeBudget + PracticeOffBudget;

            }

        }

        [NotMapped]
        public double ModuleRatingSystemBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.ModuleRatingSystemBudget;
                }
                return sum;

            }

        }

        [NotMapped]
        public double ModuleRatingSystemOffBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.ModuleRatingSystemOffBudget;
                }
                return sum;

            }

        }

        [NotMapped]
        public double ModuleRatingSystemTotal
        {

            get
            {
                return ModuleRatingSystemBudget + ModuleRatingSystemOffBudget;

            }

        }

        [NotMapped]
        public double CourseProjectBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.CourseProjectBudget;
                }
                return sum;

            }

        }

        [NotMapped]
        public double CourseProjectOffBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.CourseProjectOffBudget;
                }
                return sum;

            }

        }


        [NotMapped]
        public double CourseProjectTotal
        {

            get
            {
                return CourseProjectBudget + CourseProjectOffBudget;

            }

        }



        [NotMapped]
        public double ConsultationsBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.ConsultationsBudget;
                }
                return sum;

            }

        }

        [NotMapped]
        public double ConsultationsOffBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.ConsultationsOffBudget;
                }
                return sum;

            }

        }

        [NotMapped]
        public double ConsultationsTotal
        {

            get
            {
                return ConsultationsBudget + ConsultationsOffBudget;

            }

        }

        [NotMapped]
        public double ReviewingTestsBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.ReviewingTestsBudget;
                }
                return sum;

            }

        }

        [NotMapped]
        public double ReviewingTestsOffBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.ReviewingTestsOffBudget;
                }
                return sum;

            }

        }

        [NotMapped]
        public double ReviewingTestsTotal
        {

            get
            {
                return ReviewingTestsBudget + ReviewingTestsOffBudget;

            }

        }

        public double OffsetsBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.OffsetsBudget;
                }
                return sum;

            }

        }

        public double OffsetsOffBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.OffsetsOffBudget;
                }
                return sum;

            }

        }

        [NotMapped]
        public double OffsetsTotal
        {

            get
            {
                return OffsetsBudget + OffsetsOffBudget;

            }

        }

        public double ExamsBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.ExamsBudget;
                }
                return sum;

            }

        }

        public double ExamsOffBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.ExamsOffBudget;
                }
                return sum;

            }

        }

        [NotMapped]
        public double ExamsTotal
        {

            get
            {
                return ExamsBudget + ExamsOffBudget;

            }

        }

        public double StudyPracticeBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.EducationPracticeBudget;
                }
                return sum;

            }

        }

        public double StudyPracticeOffBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.EducationPracticeOffBudget;
                }
                return sum;

            }

        }

        [NotMapped]
        public double StudyPracticeTotal
        {

            get
            {
                return StudyPracticeBudget + StudyPracticeOffBudget;

            }

        }

        public double ProductionPracticeBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.InternshipBudget;
                }
                return sum;

            }

        }

        public double ProductionPracticeOffBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.InternshipOffBudget;
                }
                return sum;

            }

        }

        [NotMapped]
        public double ProductionPracticeTotal
        {

            get
            {
                return ProductionPracticeBudget + ProductionPracticeOffBudget;

            }

        }

        public double ThesisProjectBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.ThesisProjectBudget;
                }
                return sum;

            }

        }

        public double ThesisProjectOffBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.ThesisProjectOffBudget;
                }
                return sum;

            }

        }

        [NotMapped]
        public double ThesisProjectTotal
        {

            get
            {
                return ThesisProjectBudget + ThesisProjectOffBudget;

            }

        }


        public double StateExaminationComissionBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.StateExaminationComissionBudget;
                }
                return sum;

            }

        }

        public double StateExaminationComissionOffBudget
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.StateExaminationComissionOffBudget;
                }
                return sum;

            }

        }



        public double StateExaminationComission
        {

            get
            {
               
                return StateExaminationComissionBudget + StateExaminationComissionOffBudget;

            }

        }

        public double PostgraduateStudies//уточнить расчет
        {

            get
            {
                double sum = 0;
                foreach (StateRow row in StateRows)
                {
                    sum += row.PostgraduateStudies;
                }
                return sum;

            }

        }



        [NotMapped]
        public double TotalBudget
        {
            get
            {
                return LecturesBudget + LabsBudget + PracticeBudget + ModuleRatingSystemBudget +
                    CourseProjectBudget + ConsultationsBudget + ReviewingTestsBudget + OffsetsBudget
                    + ExamsBudget + StudyPracticeBudget + ProductionPracticeBudget + ThesisProjectBudget;
            }

        }

        [NotMapped]
        public double TotalOffBudget
        {
            get
            {
                return LecturesOffBudget + LabsOffBudget + PracticeOffBudget + ModuleRatingSystemOffBudget +
                    CourseProjectOffBudget + ConsultationsOffBudget + ReviewingTestsOffBudget + OffsetsOffBudget
                    + ExamsOffBudget + StudyPracticeOffBudget + ProductionPracticeOffBudget + ThesisProjectOffBudget;
            }

        }

        [NotMapped]
        public double Total
        {
            get
            {
                return TotalBudget + TotalOffBudget;
            }

        }


    }
}
