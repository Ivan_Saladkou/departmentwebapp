﻿using DataAccessLayer.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer
{
    public class Faculty
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Введите название факультета")]
        [Display(Name = "Название")]
        public string Name { get; set; }

        public ICollection<Department> Departments { get; set; }
    }
}