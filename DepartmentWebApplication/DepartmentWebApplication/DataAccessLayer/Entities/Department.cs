﻿using DataAccessLayer.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer
{
    public class Department
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Выберите факультет")]
        [Display(Name = "Факультет")]
        public int FacultyId { get; set; }

        [Required(ErrorMessage = "Введите название кафедры")]
        [Display(Name = "Название")]
        public string Name { get; set; }

        [Display(Name = "Сокращенное название")]
        public string NameAbbreviated { get; set; }

        public ICollection<Discipline> Disciplines { get; set; }

        
        [Display(Name = "Факультет")]
        public virtual Faculty Faculty { get; set; }
    }
}