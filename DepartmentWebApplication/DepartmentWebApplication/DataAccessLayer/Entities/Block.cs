﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities
{
    class Block
    {
        public int Id { get; set; }

        public string SequentialNumber { get; set; }
        public string Title { get; set; }
        public List<Subject> Disciplines;
    }
}
