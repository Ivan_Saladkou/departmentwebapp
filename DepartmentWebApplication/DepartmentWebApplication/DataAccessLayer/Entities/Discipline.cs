﻿using DataAccessLayer.Enums;
using DataAccessLayer.Entities.Load;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities
{
    public class Discipline
    {
        public int Id { get; set; }

        public int SubjectId { get; set; }
        public virtual Subject Subject { get; set; }

        public int PlanId { get; set; }
        public virtual Plan Plan { get; set; }

        public int? DepartmentId { get; set; }
        public virtual Department Department { get; set; }

        public ICollection<DistributionPlan> DistributionPlans { get; set; }

        //[Required(ErrorMessage = "Введите номер семестра")]
        [Display(Name = "Номер семестра")]
        public int SemesterNumber { get; set; }


       // [Required(ErrorMessage = "Выберите вид контроля")]
        [Display(Name = "Вид контроля")]
        public Control? Control { get; set; }

       // [Required(ErrorMessage = "Выберите тип курсовой")]
        [Display(Name = "Тип курсовой")]
        public CourseType? CourseType { get; set; }

        //[Required(ErrorMessage = "Введите лекции")]
        [Display(Name = "Лекции")]
        public double Lectures { get; set; }

        //[Required(ErrorMessage = "Введите лабораторные")]
        [Display(Name = "Лабораторные")]
        public double Labs { get; set; }

        //[Required(ErrorMessage = "Введите практические")]
        [Display(Name = "Практические часы")]
        public double Practices { get; set; }

        //[Required(ErrorMessage = "Введите семинарные")]
        [Display(Name = "Семинарные")]
        public double Seminars { get; set; }

        //[Required(ErrorMessage = "Введите самостоятельные")]
        [Display(Name = "Самостоятельные часы")]
        public double Independents { get; set; }

        //[Required(ErrorMessage = "Введите КСР")]
        [Display(Name = "КСР")]
        public double Ksr { get; set; }

        [Display(Name = "Потоковая дисциплина")]
        public bool IsStreaming { get; set; }

        [Display(Name = "С комиссией")]
        public bool IsWithCommission { get; set; }

        [Display(Name = "Количество членов в комиссии")]
        public int MemberAmount { get; set; }


        [NotMapped]
        public double Audits
        {
            get
            {
                return Lectures + Labs + Practices + Seminars;
            }
        }

        [NotMapped]
        public double All
        {
            get
            {
                return Audits + Independents + Ksr;
            }
        }

        [NotMapped]
        public double CourseNumber
        {
            get
            {
                return Math.Ceiling(Convert.ToDouble(SemesterNumber / 2));
            }
        }
    }
}
