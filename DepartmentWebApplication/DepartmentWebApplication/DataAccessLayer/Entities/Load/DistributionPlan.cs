﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Entities.Load
{
    public class DistributionPlan
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Выберите дисциплину")]
        [Display(Name = "Дисциплина")]
        public int DisciplineId { get; set; }

        public virtual Discipline Discipline { get; set; }

        [Required(ErrorMessage = "Выберите бюджет")]
        [Display(Name = "Бюджет")]
        public Budget Budget { get; set; }

        [Required(ErrorMessage = "Выберите форму обучения")]
        [Display(Name = "Форма обучения")]
        public FormStudy FormStudy { get; set; }

        [Required(ErrorMessage = "Выберите ступень")]
        [Display(Name = "Ступень")]
        public Stage Stage { get; set; }

        [Required(ErrorMessage = "Выберите семестр")]
        [Display(Name = "Семестр")]
        public TimeOfYear TimeOfYear { get; set; }

        public int Course { get; set; }
        public int BudgetStudents { get; set; }
        public int OffBudgetStudents { get; set; }
        public double BudgetGroups { get; set; }
        public double OffBudgetGroups { get; set; }
        [Display(Name = "Лекции")]
        public double Lectures { get; set; }
        [Display(Name = "Лабораторные работы")]
        public double Labs { get; set; }
        [Display(Name = "Практические занятия")]
        public double Practice { get; set; }
        [Display(Name = "Модульно-рейтинговая система")]
        public double ModuleRatingSystem { get; set; }
        [Display(Name = "Индивидуальные задания")]
        public double IndividualsTasks { get; set; }//пока не берется в расчет
        [Display(Name = "Курсовое проекты")]
        public double CourseProject { get; set; }
        [Display(Name = "Консультации")]
        public double Consultations { get; set; }
        [Display(Name = "Рецензирование контрольных работ")]
        public double ReviewingTests { get; set; }//Рецензирование контрольных работ
        [Display(Name = "Зачеты")]
        public double Offsets { get; set; }
        [Display(Name = "Экзамены")]
        public double Exams { get; set; }
        [Display(Name = "Учебная практика")]
        public double EducationPractice { get; set; }
        [Display(Name = "Производственная практика")]
        public double Internship { get; set; }
        [Display(Name = "Дипломное проектирование")]
        public double ThesisProject { get; set; }//Дипломное проектирование
        [Display(Name = "ГЭК")]
        public double StateExaminationComission { get; set; }//ГЭК

        [NotMapped]
        public double LectureDiscrete
        {
            get
            {
                return Lectures;
            }
        }

        [NotMapped]
        public double LabDiscrete
        {
            get
            {
                return Labs/(BudgetGroups+OffBudgetGroups);
            }
        }

    }
}
