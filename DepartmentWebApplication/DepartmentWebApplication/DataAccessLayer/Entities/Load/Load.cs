﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities.Load
{
    public class Load
    {
        public int Id { get; set; }
        public int TeacherId { get; set; }
        public virtual Teacher Teacher { get; set; }
        public virtual int DistributionPlanId { get; set; }
        public DistributionPlan DistributionPlan { get; set; }
        [Display(Name = "Лекции")]
        public double Lectures { get; set; }
        [Display(Name = "Лабораторные работы")]
        public double Labs { get; set; }
        [Display(Name = "Практические занятия")]
        public double Practice { get; set; }
        [Display(Name = "Модульно-рейтинговая система")]
        public double ModuleRatingSystem { get; set; }
        [Display(Name = "Индивидуальные задания")]
        public double IndividualsTasks { get; set; }//пока не берется в расчет
        [Display(Name = "Курсовое проекты")]
        public double CourseProject { get; set; }
        [Display(Name = "Консультации")]
        public double Consultations { get; set; }
        [Display(Name = "Рецензирование контрольных работ")]
        public double ReviewingTests { get; set; }//Рецензирование контрольных работ
        [Display(Name = "Зачеты")]
        public double Offsets { get; set; }
        [Display(Name = "Экзамены")]
        public double Exams { get; set; }
        [Display(Name = "Учебная практика")]
        public double EducationPractice { get; set; }
        [Display(Name = "Производственная практика")]
        public double Internship { get; set; }
        [Display(Name = "Дипломное проектирование")]
        public double ThesisProject { get; set; }//Дипломное проектирование
        [Display(Name = "ГЭК")]
        public double StateExaminationComission { get; set; }//ГЭК

    }
}
