﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Entities.Load
{
    public class StaticDistributionPlan
    {
        public int Id { get; set; }

        public int DistributionPlanId { get; set; }
        public virtual DistributionPlan DistributionPlan { get; set; }

        public int Course { get; set; }
        public int BudgetStudents { get; set; }
        public int OffBudgetStudents { get; set; }
        public double BudgetGroups { get; set; }
        public double OffBudgetGroups { get; set; }
        public double Lectures { get; set; }
        public double Labs { get; set; }
        public double Practice { get; set; }
        public double ModuleRatingSystem { get; set; }
        public double IndividualsTasks { get; set; }//пока не берется в расчет
        public double CourseProject { get; set; }
        public double Consultations { get; set; }
        public double ReviewingTests { get; set; }//Рецензирование контрольных работ
        public double Offsets { get; set; }
        public double Exams { get; set; }
        public double EducationPractice { get; set; }
        public double Internship { get; set; }
        public double ThesisProject { get; set; }//Дипломное проектирование
        public double StateExaminationComission { get; set; }//ГЭК

        [NotMapped]
        public double LectureDiscrete
        {
            get
            {
                return Lectures;
            }
        }

        [NotMapped]
        public double LabDiscrete
        {
            get
            {
                return Labs/(BudgetGroups+OffBudgetGroups);
            }
        }

        [NotMapped]
        public double PracticeDiscrete
        {
            get
            {
                return Practice / (BudgetGroups + OffBudgetGroups);
            }
        }

        [NotMapped]
        public double ModuleRatingSystemDiscrete
        {
            get
            {
                return ModuleRatingSystem; //Все зависят от лектора
            }
        }

        [NotMapped]
        public double IndividualsTasksDiscrete
        {
            get
            {
                return IndividualsTasks / (BudgetStudents + OffBudgetStudents); //по студенту
            }
        }

        [NotMapped]
        public double CourseProjectDiscrete
        {
            get
            {
                return CourseProject / (BudgetStudents+OffBudgetStudents);
            }
        }

        [NotMapped]
        public double ConsultationsDiscrete
        {
            get
            {
                return Consultations; //Лектору
            }
        }

        [NotMapped]
        public double ReviewingTestsDiscrete
        {
            get
            {
                return ReviewingTests / (BudgetStudents + OffBudgetStudents); //по студентам
            }
        }

        [NotMapped]
        public double OffsetsDiscrete
        {
            get
            {
                return Offsets; //все 0.35
            }
        }

        [NotMapped]
        public double ExamsDiscrete
        {
            get
            {
                return Exams; //все сумма на колчиство членов комисии  0.5
            }
        }

        [NotMapped]
        public double EducationPracticeDiscrete
        {
            get
            {
                return EducationPractice / (BudgetGroups + OffBudgetGroups); //по подгруппе
            }
        }

        [NotMapped]
        public double InternshipDiscrete
        {
            get
            {
                return Internship / (BudgetStudents + OffBudgetStudents); // по человеку
            }
        }

        [NotMapped]
        public double ThesisProjectDiscrete
        {
            get
            {
                return ThesisProject / (BudgetStudents + OffBudgetStudents); //по человеку
            }
        }

        [NotMapped]
        public double StateExaminationComissionDiscrete
        {
            get
            {
                return StateExaminationComission; // так как с экзаменами
            }
        }


        // Руководство магистрантами по человеку


        public Dictionary<string, double> GetDiscretesDict()
        {
            Dictionary<string, double> discretesDict = new Dictionary<string, double>();
            discretesDict.Add("Lectures", LectureDiscrete);
            discretesDict.Add("Labs", LabDiscrete);
            discretesDict.Add("Practice", PracticeDiscrete);
            discretesDict.Add("ModuleRatingSystem", ModuleRatingSystemDiscrete);
            discretesDict.Add("IndividualsTasks", IndividualsTasksDiscrete);
            discretesDict.Add("CourseProject", CourseProjectDiscrete);
            discretesDict.Add("Consultations", ConsultationsDiscrete);
            discretesDict.Add("ReviewingTests", ReviewingTestsDiscrete);
            discretesDict.Add("Offsets", OffsetsDiscrete);
            discretesDict.Add("Exams", ExamsDiscrete);
            discretesDict.Add("EducationPractice", EducationPracticeDiscrete);
            discretesDict.Add("Internship", InternshipDiscrete);
            discretesDict.Add("ThesisProject", ThesisProjectDiscrete);
            discretesDict.Add("StateExaminationComission", StateExaminationComissionDiscrete);
            return discretesDict;
        }

    }
}
