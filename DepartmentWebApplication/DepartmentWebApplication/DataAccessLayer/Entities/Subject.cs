﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DataAccessLayer.Enums;

namespace DataAccessLayer.Entities
{
    public class Subject
    {
        public int Id { get; set; }


        [Required(ErrorMessage = "Введите название дисциплины")]
        [Display(Name = "Дисциплина")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Введите сокращенное название дисциплины")]
        [Display(Name = "Сокращенное название")]
        public string NameAbbreviated { get; set; }


        [NotMapped]
        public string Exams
        {
            get
            {
                return string.Join(",", DisciplineSemesters
                    .Where(d => d.Control == Control.Exam)
                    .Select(d => d.SemesterNumber));
            }
        }

        [NotMapped]
        public string Credits
        {
            get
            {
                return string.Join(",", DisciplineSemesters
                    .Where(d => d.Control == Control.Credit)
                    .Select(d => d.SemesterNumber));
            }
        }

        [NotMapped]
        public string Works
        {
            get
            {
                return string.Join(",", DisciplineSemesters
                    .Where(d => d.CourseType == CourseType.Work)
                    .Select(d => d.SemesterNumber));
            }
        }

        [NotMapped]
        public string Projects
        {
            get
            {
                return string.Join(",", DisciplineSemesters
                    .Where(d => d.CourseType == CourseType.Project)
                    .Select(d => d.SemesterNumber));
            }
        }

        [NotMapped]
        public double Lectures
        {
            get
            {
                return DisciplineSemesters.Sum(d => d.Lectures);
            }
        }

        [NotMapped]
        public double Labs
        {
            get
            {
                return DisciplineSemesters.Sum(d => d.Labs);
            }
        }

        [NotMapped]
        public double Practices
        {
            get
            {
                return DisciplineSemesters.Sum(d => d.Practices);
            }
        }

        [NotMapped]
        public double Seminars
        {
            get
            {
                return DisciplineSemesters.Sum(d => d.Seminars);
            }
        }

        [NotMapped]
        public double Audits
        {
            get
            {
                return DisciplineSemesters.Sum(d => d.Audits);
            }
        }

        [NotMapped]
        public double Independents
        {
            get
            {
                return DisciplineSemesters.Sum(d => d.Independents);
            }
        }

        [NotMapped]
        public double Ksr
        {
            get
            {
                return DisciplineSemesters.Sum(d => d.Ksr);
            }
        }

        [NotMapped]
        public double All
        {
            get
            {
                return DisciplineSemesters.Sum(d => d.All);
            }
        }

        public ICollection<Discipline> DisciplineSemesters { get; set; } = new List<Discipline>();
    }
}