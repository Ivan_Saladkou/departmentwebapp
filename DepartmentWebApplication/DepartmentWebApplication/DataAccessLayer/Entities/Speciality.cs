﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities
{
    public class Speciality
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Введите название специальности")]
        [Display(Name = "Специальность")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Введите сокращенное название специальности")]
        [Display(Name = "Сокращенное название")]
        public string NameAbbreviated { get; set; }

        [Required(ErrorMessage = "Введите шифр")]
        [Display(Name = "Шифр")]
        public string Cipher { get; set; }


        public ICollection<Contingent> Contingents { get; set; }
        public ICollection<Plan> Plans { get; set; }
    }
}
