﻿using DataAccessLayer.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.Entities
{
    public class Plan
    {
        public int Id { get; set; }

        public int SpecialityId { get; set; }
        public virtual Speciality Speciality { get; set; }

        [Required(ErrorMessage = "Введите название специализации")]
        [Display(Name = "Специализация")]
        public string Specialization { get; set; }

        [Required(ErrorMessage = "Введите срок обучения (лет)")]
        [Display(Name = "Срок обучения (лет)")]
        public double Duration { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        [Required(ErrorMessage = "Введите дату ввода")]
        [Display(Name = "Дата ввода")]
        public DateTime Date { get; set; }

        [Required(ErrorMessage = "Введите регистрационный номер")]
        [Display(Name = "Регистрационный номер")]
        public string RegNumber { get; set; }

        [Required(ErrorMessage = "Выберите ступень обучения")]
        [Display(Name = "Ступень обучения")]
        public Stage Stage { get; set; }

        [Required(ErrorMessage = "Выберите форму обучения")]
        [Display(Name = "Форма обучения ")]
        public FormStudy FormStudy { get; set; }

        [Required(ErrorMessage = "Выберите бюджет")]
        [Display(Name = "Бюджет ")]
        public Budget Budget { get; set; }


        public ICollection<Discipline> Disciplines { get; set; }
        public ICollection<Semester> Semesters { get; set; }
        public ICollection<Practice> Practices { get; set; }
    }
}