﻿using DataAccessLayer.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities
{
    public class Coefficient
    {
        public int Id { get; set; }
        public WorkType Name { get; set; }
        public double Value { get; set; }
    }
}
