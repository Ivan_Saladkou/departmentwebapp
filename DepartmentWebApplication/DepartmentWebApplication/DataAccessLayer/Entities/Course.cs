﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities
{
    public class Course
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public int Budget { get; set; }
        public int OffBudget { get; set; }
        public int Foreign { get; set; }
        public double BudgetGroups { get; set; }
        public double OffBudgetGroups { get; set; }

        public int ContingentId { get; set; }
        public Contingent Contingent { get; set; }

        [NotMapped]
        public double BudgetSubgroups {
            get {
                return BudgetGroups / 0.5;
            }
        }

        [NotMapped]
        public double OffBudgetSubgroups
        {
            get
            {
                return OffBudgetGroups / 0.5;
            }
        }
    }
}
