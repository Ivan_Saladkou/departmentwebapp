﻿using DataAccessLayer.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities
{
    public class Practice
    {
        public int Id { get; set; }
        public PracticeType PracticeType { get; set; }
        public int Course { get; set; }
        public int Duration { get; set; }
        public int PlanId { get; set; }
        public Plan Plan { get; set; }
    }
}
