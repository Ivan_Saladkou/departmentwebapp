﻿using DataAccessLayer.Repositories;
using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IDisciplineRepository : IRepository<Subject>
    {
        int AddGetId(Subject discipline);
    }
}
