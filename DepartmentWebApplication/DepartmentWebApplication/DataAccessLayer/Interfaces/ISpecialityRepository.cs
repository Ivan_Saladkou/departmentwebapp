﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories;

namespace DataAccessLayer.Interfaces
{
    public interface ISpecialityRepository : IRepository<Speciality>
    {
        int AddGetId(Speciality speciality);
    }
}
