﻿using DataAccessLayer.Entities;
using DataAccessLayer.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Service
{
    public class CoefficientService
    {
        private DepartmentEntities db = new DepartmentEntities();

        public double GetValueByName(WorkType name)
        {
            return db.Coefficients.Where(c => c.Name == name).First().Value;
        }

    }
}
