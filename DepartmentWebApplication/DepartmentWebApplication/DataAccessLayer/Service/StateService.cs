﻿using DataAccessLayer.Entities;
using DataAccessLayer.Entities.Load;
using DataAccessLayer.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;


namespace DataAccessLayer.Service
{
    public class StateService
    {
        private DepartmentEntities db = new DepartmentEntities();
        private CoefficientService coefficientService = new CoefficientService();

        private List<string> DiplomaDisciplines = new List<string> {
            "Преддипломная практика",
            "Дипломное проектирование",
            "Председатель ГЭК",
            "Члены ГЭК",
            "Нормаконтроль",
            "Утверждение",
            "Рецензирование"
        };


        public StateDocument FillStateDocument(StateDocument stateDocument)
        {


            db.StateBlocks.Add(new StateBlock { Stage = Stage.First, FormStudy = FormStudy.FullTime, TimeOfYear = TimeOfYear.Autumn, StateDocumentId = stateDocument.Id });
            db.StateBlocks.Add(new StateBlock { Stage = Stage.First, FormStudy = FormStudy.Extramural, TimeOfYear = TimeOfYear.Autumn, StateDocumentId = stateDocument.Id });
            db.StateBlocks.Add(new StateBlock { Stage = Stage.First, FormStudy = FormStudy.FullTime, TimeOfYear = TimeOfYear.Spring, StateDocumentId = stateDocument.Id });
            db.StateBlocks.Add(new StateBlock { Stage = Stage.First, FormStudy = FormStudy.Extramural, TimeOfYear = TimeOfYear.Spring, StateDocumentId = stateDocument.Id });
            db.SaveChanges();

            StateDocument state = db.StateDocumentes.Include(s => s.Department)
                                                    .Include(s => s.StateBlocks
                                                                    .Select(b => b.StateRows))
                                                    .First(s => s.Id == stateDocument.Id);


            List<Plan> plans = db.Plans
              .Include(p => p.Disciplines.Select(dis => dis.Subject))
              .Include(p => p.Speciality.Contingents.Select(c => c.Courses))
              .Include(p => p.Practices)
              .Where(p => p.Date.Year == state.Date.Year)
              .Where(p => p.Budget == state.Budget)
              .ToList();

            foreach (Plan plan in plans)
            {
                if (plan.Speciality.Contingents.Count() != 0)
                {
                    foreach (Discipline discipline in plan.Disciplines.Where(dis => dis.DepartmentId == state.DepartmentId))
                    {
                        StateBlock block = GetBlock(state, discipline);
                        StateRow row = GetRow(discipline);

                        block.StateRows.Add(row);
                        SaveDistribution(row, discipline, block, plan);
                    }
                    
                }

            }
            db.SaveChanges();


            return state;
        }



        public StateRow GetRow(Discipline discipline)
        {
            Contingent contingent = discipline.Plan.Speciality.Contingents.First();// добавить логику выбора контингента по нужному году а не просто первого
            int courseNumber = Convert.ToInt32(Math.Ceiling(discipline.SemesterNumber / 2.0));
            Course course = contingent.Courses.ElementAt(courseNumber - 1);

            StateRow row = new StateRow
            {
                DisciplineName = discipline.Subject.Name,
                SpecialityName = discipline.Plan.Speciality.NameAbbreviated,
                Course = courseNumber,
                BudgetStudents = course.Budget,
                OffBudgetStudents = course.OffBudget,
                Streams = (discipline.IsStreaming) ? 1 : 0,
                BudgetGroups = course.BudgetGroups,
                OffBudgetGroups = course.OffBudgetGroups,
                BudgetSubGroups = course.BudgetSubgroups,
                OffBudgetSubGroups = course.OffBudgetSubgroups
            };

            if (DiplomaDisciplines.Contains(row.DisciplineName))
            {
                return SetDiplomaDisciplineValues(row, discipline);
            }
            if (row.DisciplineName.Contains("практика"))
            {
                return SetPracticeValues(row, discipline);
            }
            SetBasicDisciplineValues(row, discipline);
            return row;
        }


        private StateRow SetPracticeValues(StateRow row, Discipline discipline)
        {
            Contingent contingent = discipline.Plan.Speciality.Contingents.First();// добавить логику выбора контингента по нужному году а не просто первого
            int courseNumber = Convert.ToInt32(Math.Ceiling(discipline.SemesterNumber / 2.0));
            Course course = contingent.Courses.ElementAt(courseNumber - 1);
            Practice practice;
            try
            {
                practice = discipline.Plan.Practices.First(p => p.Course == courseNumber);
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine("Практика не установлена в плане " + ex.Message);
                return row;
            }
            if (practice.PracticeType == PracticeType.Educational)
            {
                row.EducationPracticeBudget = coefficientService.GetValueByName(WorkType.EducationalPractice) * course.BudgetSubgroups;
                row.EducationPracticeOffBudget = coefficientService.GetValueByName(WorkType.EducationalPractice) * course.OffBudgetSubgroups;
            }
            else
            {
                row.InternshipBudget = coefficientService.GetValueByName(WorkType.Internship) * course.Budget * practice.Duration;
                row.InternshipOffBudget = coefficientService.GetValueByName(WorkType.Internship) * course.OffBudget * practice.Duration;
            }
            return row;
        }


        private void SaveDistribution(StateRow row, Discipline discipline, StateBlock state, Plan plan)
        {
            DistributionPlan distributionPlan = new DistributionPlan {
                Discipline = discipline,
                FormStudy = state.FormStudy,
                Stage = state.Stage,
                TimeOfYear = state.TimeOfYear,
                Budget = plan.Budget,
                Course = row.Course,
                BudgetStudents = row.BudgetStudents,
                OffBudgetStudents = row.OffBudgetStudents,
                BudgetGroups = row.BudgetGroups,
                OffBudgetGroups = row.OffBudgetGroups,
                Lectures = row.LecturesPlan,
                Labs = row.LabsPlan,
                Practice = row.PracticePlan,
                ModuleRatingSystem = row.ModuleRatingSystemBudget + row.ModuleRatingSystemOffBudget,
                CourseProject = row.CourseProjectBudget + row.CourseProjectOffBudget,
                Consultations = row.ConsultationsBudget + row.ConsultationsOffBudget,
                ReviewingTests = row.ReviewingTestsBudget + row.ReviewingTestsOffBudget,
                Offsets = row.OffsetsBudget + row.OffsetsOffBudget,
                Exams = row.ExamsBudget + row.ExamsOffBudget
            };
            StaticDistributionPlan staticDistribution = new StaticDistributionPlan
            {
                Course = distributionPlan.Course,
                BudgetStudents = distributionPlan.BudgetStudents,
                OffBudgetStudents = distributionPlan.OffBudgetStudents,
                BudgetGroups = distributionPlan.BudgetGroups,
                OffBudgetGroups = distributionPlan.OffBudgetGroups,
                Lectures = distributionPlan.Lectures,
                Labs = distributionPlan.Labs,
                Practice = distributionPlan.Practice,
                ModuleRatingSystem = distributionPlan.ModuleRatingSystem,
                CourseProject = distributionPlan.CourseProject,
                Consultations = distributionPlan.Consultations,
                ReviewingTests = distributionPlan.ReviewingTests,
                Offsets = distributionPlan.Offsets,
                Exams = distributionPlan.Exams,
                DistributionPlan = distributionPlan
            };

            db.DistributionPlans.Add(distributionPlan);
            db.StaticDistributionPlans.Add(staticDistribution);
            db.SaveChanges();
        }


        private StateRow SetBasicDisciplineValues(StateRow row, Discipline discipline)
        {
            Contingent contingent = discipline.Plan.Speciality.Contingents.First();// добавить логику выбора контингента по нужному году а не просто первого
            int courseNumber = Convert.ToInt32(Math.Ceiling(discipline.SemesterNumber / 2.0));
            Course course = contingent.Courses.ElementAt(courseNumber - 1);
            row.LecturesPlan = discipline.Lectures;
            row.Lectures = discipline.Lectures * row.Streams;
            row.LabsPlan = discipline.Labs;
            row.LabsBudget = discipline.Labs * course.BudgetSubgroups;
            row.LabsOffBudget = discipline.Labs * course.OffBudgetSubgroups;
            row.PracticePlan = discipline.Practices;
            row.PracticeBudget = discipline.Practices * Math.Ceiling(course.BudgetGroups);
            row.PracticeOffBudget = discipline.Practices * Math.Ceiling(course.OffBudgetGroups);
            row.ModuleRatingSystemBudget = coefficientService.GetValueByName(WorkType.RatingControl) * course.Budget;
            row.ModuleRatingSystemOffBudget = coefficientService.GetValueByName(WorkType.RatingControl) * course.OffBudget;
            row.CourseProjectBudget = course.Budget * CalculateCourseProject(discipline.CourseType);
            row.CourseProjectOffBudget = course.OffBudget * CalculateCourseProject(discipline.CourseType);
            row.ConsultationsBudget = (discipline.Control == Control.Exam) ? course.BudgetGroups * coefficientService.GetValueByName(WorkType.Consultation) : 0;
            row.ConsultationsOffBudget = (discipline.Control == Control.Exam) ? course.OffBudgetGroups * coefficientService.GetValueByName(WorkType.Consultation) : 0;
            row.ReviewingTestsBudget = course.Budget * coefficientService.GetValueByName(WorkType.ReviewingTests);
            row.ReviewingTestsOffBudget = course.OffBudget * coefficientService.GetValueByName(WorkType.ReviewingTests);
            row.OffsetsBudget = (discipline.Control == Control.Credit) ? course.Budget * coefficientService.GetValueByName(WorkType.Offset) : 0;
            row.OffsetsOffBudget = (discipline.Control == Control.Credit) ? course.OffBudget * coefficientService.GetValueByName(WorkType.Offset) : 0;
            row.ExamsBudget = (discipline.Control == Control.Exam) ? course.Budget * coefficientService.GetValueByName(WorkType.Exam) : 0;
            row.ExamsOffBudget = (discipline.Control == Control.Exam) ? course.OffBudget * coefficientService.GetValueByName(WorkType.Exam) : 0;


            //Добавить логику аспирант
            return row;
        }

        private double CalculateCourseProject(CourseType? courseType)
        {
            if (!courseType.HasValue) {
                return 0;
            }
            WorkType workType = (courseType == CourseType.Project) ? WorkType.CourseProject : WorkType.CourseWork;
            return coefficientService.GetValueByName(workType);
        }

        private StateRow SetDiplomaDisciplineValues(StateRow row, Discipline discipline)
        {
            switch (row.DisciplineName)
            {

                case "Председатель ГЭК":
                    row.StateExaminationComissionBudget = coefficientService.GetValueByName(WorkType.SecChairman) * row.BudgetStudents;
                    row.StateExaminationComissionOffBudget = coefficientService.GetValueByName(WorkType.SecChairman) * row.OffBudgetStudents;
                    break;
                case "Члены ГЭК":
                    int amount = discipline.Plan.Disciplines.First(discipline => discipline.Subject.Name == "Дипломное проектирование").MemberAmount;
                    row.StateExaminationComissionBudget = coefficientService.GetValueByName(WorkType.SecMember) * row.BudgetStudents * amount;
                    row.StateExaminationComissionOffBudget = coefficientService.GetValueByName(WorkType.SecMember) * row.OffBudgetStudents* amount;
                    break;
                case "Нормоконтроль":
                    row.ThesisProjectBudget = coefficientService.GetValueByName(WorkType.SecNormControl) * row.BudgetStudents;
                    row.ThesisProjectOffBudget = coefficientService.GetValueByName(WorkType.SecNormControl) * row.OffBudgetStudents;
                    break;
                case "Утверждение":
                    row.ThesisProjectBudget = coefficientService.GetValueByName(WorkType.SecApproval) * row.BudgetStudents;
                    row.ThesisProjectOffBudget = coefficientService.GetValueByName(WorkType.SecApproval) * row.OffBudgetStudents;
                    break;
                case "Рецензирование":
                    row.ThesisProjectBudget = coefficientService.GetValueByName(WorkType.SecReview) * row.BudgetStudents;
                    row.ThesisProjectOffBudget = coefficientService.GetValueByName(WorkType.SecReview) * row.OffBudgetStudents;
                    break;
                case "Дипломное проектирование":
                    if (discipline.Plan.Budget == Budget.Rb)
                    {
                        row.ThesisProjectBudget = coefficientService.GetValueByName(WorkType.ThesisProjectBelorussian) * row.BudgetStudents;
                        row.ThesisProjectOffBudget = coefficientService.GetValueByName(WorkType.ThesisProjectBelorussian) * row.OffBudgetStudents;
                    }
                    else
                    {
                        row.ThesisProjectBudget = coefficientService.GetValueByName(WorkType.ThesisProjectRussian) * row.BudgetStudents;
                        row.ThesisProjectOffBudget = coefficientService.GetValueByName(WorkType.ThesisProjectRussian) * row.OffBudgetStudents;
                    }
                    break;
            }
            return row;
        }

        private StateBlock GetBlock(StateDocument state, Discipline discipline)
        {
            TimeOfYear timeOfYear = (discipline.SemesterNumber % 2 == 0) ? TimeOfYear.Spring : TimeOfYear.Autumn;
            foreach (StateBlock block in state.StateBlocks)
            {
                if (block.FormStudy == discipline.Plan.FormStudy && block.TimeOfYear == timeOfYear)
                {
                    return block;
                }
            }
            return null;
        }
    }
}
