﻿namespace DataAccessLayer
{
    using DataAccessLayer.Entities;
    using DataAccessLayer.Entities.Load;
    using DataAccessLayer.Enums;
    using DataAccessLayer.Extensions;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class DepartmentEntities : DbContext
    {
        public DepartmentEntities()
            : base("name=DepartmentEntities")
        {
            Database.SetInitializer(new MyContextInitializer());
        }

        public virtual DbSet<Plan> Plans { get; set; }
        public virtual DbSet<Subject> Subjects { get; set; }
        public virtual DbSet<Faculty> Faculties { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Semester> Semesters { get; set; }
        public virtual DbSet<Coefficient> Coefficients { get; set; }
        public virtual DbSet<Speciality> Specialities { get; set; }
        public virtual DbSet<Course> Courses { get; set; }
        public virtual DbSet<Contingent> Contingents { get; set; }
        public virtual DbSet<Teacher> Teachers { get; set; }
        public virtual DbSet<Discipline> Disciplines { get; set; }
        public virtual DbSet<StateDocument> StateDocumentes { get; set; }
        public virtual DbSet<StateBlock> StateBlocks { get; set; }
        public virtual DbSet<StateRow> StateRows { get; set; }
        public virtual DbSet<Practice> Practices { get; set; }
        public virtual DbSet<Setting> Settings { get; set; }
        public virtual DbSet<DistributionPlan> DistributionPlans { get; set; }
        public virtual DbSet<StaticDistributionPlan> StaticDistributionPlans { get; set; }
        public virtual DbSet<Load> Loads { get; set; }
    }


    class MyContextInitializer : CreateDatabaseIfNotExists<DepartmentEntities>
    {
        protected override void Seed(DepartmentEntities db)
        {

            if (db.Specialities.ToList().Count != 0) return;
            #region Specialities
            db.Specialities.Add(new Speciality
            {
                Name = "Программная инженерия",
                NameAbbreviated = "ПИр",
                Cipher = "09.03.04"

            });
            db.Specialities.Add(new Speciality
            {
                Name = "Инноватика",
                NameAbbreviated = "УИР",
                Cipher = "27.03.05"

            });
            db.Specialities.Add(new Speciality
            {
                Name = "Машиностроение",
                NameAbbreviated = "М",
                Cipher = "15.03.01"

            });
            #endregion
            db.SaveChanges(); //обязательно, чтоб зависящие классы могли использовать ключ

            #region Faculties
            db.Faculties.Add(new Faculty
            {
                Name = "Инженерно-экономический"
            });
            db.Faculties.Add(new Faculty
            {
                Name = "Экономический"
            });
            db.Faculties.Add(new Faculty
            {
                Name = "Строительный"
            });
            db.Faculties.Add(new Faculty
            {
                Name = "Электротехнический"
            });
            #endregion
            db.SaveChanges();

            #region Plans
            db.Plans.Add(new Plan
            {
                SpecialityId = 1,
                Specialization = "Программная инженерия",
                Date = new DateTime(2017, 09, 01),
                Duration = 4,
                RegNumber = "030103",
                Stage = Enums.Stage.First,
                FormStudy = Enums.FormStudy.FullTime,
                Budget = Enums.Budget.Rf
            });
            db.Plans.Add(new Plan
            {
                SpecialityId = 2,
                Specialization = "Инноватика",
                Date = new DateTime(2017, 09, 01),
                Duration = 4,
                RegNumber = "030103",
                Stage = Enums.Stage.First,
                FormStudy = Enums.FormStudy.Extramural,
                Budget = Enums.Budget.Rf
            });
            db.Plans.Add(new Plan
            {
                SpecialityId = 3,
                Specialization = "Машиностроение",
                Date = new DateTime(2017, 09, 01),
                Duration = 4,
                RegNumber = "030103",
                Stage = Enums.Stage.Second,
                FormStudy = Enums.FormStudy.FullTime,
                Budget = Enums.Budget.Rb
            });
            #endregion
            db.SaveChanges();

            #region Departments
            db.Departments.Add(new Department
            {
                Name = "Автоматизированные системы управления",
                FacultyId = 1
            });
            db.Departments.Add(new Department
            {
                Name = "Экономика и управление",
                FacultyId = 1
            });
            #endregion
            db.SaveChanges();

            #region Subjects
            db.Subjects.Add(new Subject
            {
                Name = "Дипломное проектирование",
                NameAbbreviated = "ДП"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Председатель ГЭК",
                NameAbbreviated = "ПГ"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Члены ГЭК",
                NameAbbreviated = "ЧГ"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Нормаконтроль",
                NameAbbreviated = "Н"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Утверждение",
                NameAbbreviated = "У"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Рецензирование",
                NameAbbreviated = "Р"
            });

            db.Subjects.Add(new Subject
            {
                Name = "Экономическая теория ",
                NameAbbreviated = "ЭТ"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Философия",
                NameAbbreviated = "Ф"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Русский язык делового общения",
                NameAbbreviated = "РЯДО"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Иностранный язык",
                NameAbbreviated = "ИЯ"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Социология",
                NameAbbreviated = "С"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Политология",
                NameAbbreviated = "П"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Экономика предприятия",
                NameAbbreviated = "ЭП"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Правоведение",
                NameAbbreviated = "П"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Инженерная психология",
                NameAbbreviated = "ИП"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Религиоведение",
                NameAbbreviated = "Р"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Этика делового общения",
                NameAbbreviated = "ЭДО"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Психология межличностного общения",
                NameAbbreviated = "ПМО"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Основы социального государства",
                NameAbbreviated = "ОСГ"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Культурология",
                NameAbbreviated = "К"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Математика",
                NameAbbreviated = "М"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Информационные технологии",
                NameAbbreviated = "ИТ"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Физика и естествознание",
                NameAbbreviated = "ФИЕ"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Теория и системы управления",
                NameAbbreviated = "ТИСУ"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Химия и материаловедение",
                NameAbbreviated = "ХИМ"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Теория вероятностей и математическая статистика",
                NameAbbreviated = "ТВИМС"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Статистика",
                NameAbbreviated = "С"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Математические методы и модели",
                NameAbbreviated = "ММИМ"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Теория и технология программирования",
                NameAbbreviated = "ТИТП"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Теория оценивания и квалиметрии ",
                NameAbbreviated = "ТОИК"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Экология",
                NameAbbreviated = "Э"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Базы данных",
                NameAbbreviated = "БД"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Многомерный регрессионный анализ в экономике",
                NameAbbreviated = "МРАВЭ"
            });
            db.Subjects.Add(new Subject
            {
                Name = " Эконометрика",
                NameAbbreviated = "Э"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Компьютерное моделирование и современные методы оптимизации",
                NameAbbreviated = "КМИСМО"
            });
            db.Subjects.Add(new Subject
            {
                Name = " Имитационное моделирование производственных процессов",
                NameAbbreviated = "ИМПП"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Системный анализ и принятие решений",
                NameAbbreviated = "САИПР"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Механика и технологии",
                NameAbbreviated = "МИТ"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Электротехника и электроника",
                NameAbbreviated = "ЭИЭ"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Инженерная графика",
                NameAbbreviated = "ИГ"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Алгоритмы решения нестандартных задач",
                NameAbbreviated = "АРНЗ"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Технологические основы инновационной деятельности",
                NameAbbreviated = "ТОИД"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Промышленные технологии и инновации",
                NameAbbreviated = "ПТИИ"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Безопасность жизнедеятельности",
                NameAbbreviated = "БЖ"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Метрология",
                NameAbbreviated = "М"
            });
            db.Subjects.Add(new Subject
            {
                Name = " стандартизация и сертификация",
                NameAbbreviated = "СИС"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Экономико-управленческие основы инновационной деятельности",
                NameAbbreviated = "ЭОИД"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Теоретическая инноватика",
                NameAbbreviated = "ТИ"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Экономика и финансовое обеспечение инновационной деятельности",
                NameAbbreviated = "ЭИФОИД"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Управление инновационной деятельностью",
                NameAbbreviated = "УИД"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Маркетинг в инновационной сфере",
                NameAbbreviated = "МВИС"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Управление инновационными проектами",
                NameAbbreviated = "УИП"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Технологии нововведений",
                NameAbbreviated = "ТН"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Инженерные основы инновационной деятельности",
                NameAbbreviated = "ИОИД"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Введение в инноватику",
                NameAbbreviated = "ВВИ"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Основы проектирования и конструирования",
                NameAbbreviated = "ОПИК"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Производственные технологии и оборудование машиностроительного производства",
                NameAbbreviated = "ПТИОМП"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Организация труда и управление персоналом",
                NameAbbreviated = "ОТИУП"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Организация производства",
                NameAbbreviated = "ОП"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Планирование на предприятии",
                NameAbbreviated = "ПНП"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Системный анализ деятельности предприятий",
                NameAbbreviated = "САДП"
            });
            db.Subjects.Add(new Subject
            {
                Name = " Анализ финансово-хозяйственной деятельности предприятия",
                NameAbbreviated = "АФДП"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Инфраструктура нововведений",
                NameAbbreviated = "ИН"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Государственное управление инновационными процессами",
                NameAbbreviated = "ГУИП"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Производственная логистика",
                NameAbbreviated = "ПЛ"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Логистическая структура предприятий",
                NameAbbreviated = "ЛСП"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Правовое обеспечение инновационной деятельности",
                NameAbbreviated = "ПОИД"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Основы управления интеллектуальной собственностью",
                NameAbbreviated = "ОУИС"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Стратегический менеджмент",
                NameAbbreviated = "СМ"
            });
            db.Subjects.Add(new Subject
            {
                Name = " Антикризисное управление предприятием",
                NameAbbreviated = "АУП"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Автоматизированные системы управления производством",
                NameAbbreviated = "АСУП"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Построение систем управления производством",
                NameAbbreviated = "ПСУП"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Методы оценивания и минимизации рисков",
                NameAbbreviated = "МОИМР"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Управление рисками в производственной деятельности",
                NameAbbreviated = "УРВПД"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Физическая культура",
                NameAbbreviated = "ФК"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Учебная практика",
                NameAbbreviated = "УП"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Первая производственная практика",
                NameAbbreviated = "ППП"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Вторая производственная практика",
                NameAbbreviated = "ВПП"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Преддипломная практика",
                NameAbbreviated = "ПП"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Введение в специальность",
                NameAbbreviated = "ВВС"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Коррупция и ее общественная опасность",
                NameAbbreviated = "КИЕОО"
            });
            db.Subjects.Add(new Subject
            {
                Name = "Риторика",
                NameAbbreviated = "Р"
            });

            #endregion
            db.SaveChanges();

            #region Disciplines
            db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 1,
                SubjectId = 12,
                SemesterNumber = 6,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 13,
                Labs = 7,
                Practices = 2,
                Seminars = 36,
                Independents = 6,
                Ksr = 9,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 1,
                SubjectId = 39,
                SemesterNumber = 2,
                Control = Control.Credit,
                CourseType = CourseType.Project,
                Lectures = 18,
                Labs = 22,
                Practices = 2,
                Seminars = 7,
                Independents = 30,
                Ksr = 29,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 1,
                SubjectId = 11,
                SemesterNumber = 4,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 30,
                Labs = 30,
                Practices = 18,
                Seminars = 21,
                Independents = 9,
                Ksr = 19,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 2,
                SubjectId = 27,
                SemesterNumber = 5,
                Control = Control.Credit,
                CourseType = CourseType.Work,
                Lectures = 10,
                Labs = 16,
                Practices = 31,
                Seminars = 7,
                Independents = 3,
                Ksr = 18,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 2,
                SubjectId = 25,
                SemesterNumber = 1,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 12,
                Labs = 24,
                Practices = 38,
                Seminars = 20,
                Independents = 2,
                Ksr = 30,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 2,
                SubjectId = 40,
                SemesterNumber = 4,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 28,
                Labs = 11,
                Practices = 12,
                Seminars = 6,
                Independents = 30,
                Ksr = 9,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 2,
                DepartmentId = 2,
                SubjectId = 21,
                SemesterNumber = 2,
                Control = Control.Exam,
                CourseType = CourseType.Work,
                Lectures = 40,
                Labs = 10,
                Practices = 38,
                Seminars = 37,
                Independents = 39,
                Ksr = 32,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 2,
                SubjectId = 50,
                SemesterNumber = 5,
                Control = Control.Exam,
                CourseType = CourseType.Work,
                Lectures = 25,
                Labs = 6,
                Practices = 34,
                Seminars = 31,
                Independents = 2,
                Ksr = 37,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 1,
                SubjectId = 15,
                SemesterNumber = 4,
                Control = Control.Credit,
                CourseType = CourseType.Work,
                Lectures = 5,
                Labs = 40,
                Practices = 31,
                Seminars = 22,
                Independents = 17,
                Ksr = 17,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 2,
                SubjectId = 17,
                SemesterNumber = 8,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 17,
                Labs = 4,
                Practices = 6,
                Seminars = 14,
                Independents = 27,
                Ksr = 6,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 2,
                SubjectId = 28,
                SemesterNumber = 4,
                Control = Control.Credit,
                CourseType = CourseType.Project,
                Lectures = 24,
                Labs = 35,
                Practices = 25,
                Seminars = 8,
                Independents = 15,
                Ksr = 19,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 2,
                SubjectId = 43,
                SemesterNumber = 2,
                Control = Control.Exam,
                CourseType = CourseType.Work,
                Lectures = 19,
                Labs = 3,
                Practices = 14,
                Seminars = 34,
                Independents = 23,
                Ksr = 8,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 2,
                DepartmentId = 2,
                SubjectId = 42,
                SemesterNumber = 4,
                Control = Control.Credit,
                CourseType = CourseType.Work,
                Lectures = 33,
                Labs = 8,
                Practices = 1,
                Seminars = 2,
                Independents = 12,
                Ksr = 21,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 2,
                DepartmentId = 1,
                SubjectId = 45,
                SemesterNumber = 8,
                Control = Control.Exam,
                CourseType = CourseType.Work,
                Lectures = 11,
                Labs = 31,
                Practices = 5,
                Seminars = 3,
                Independents = 27,
                Ksr = 7,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 2,
                DepartmentId = 2,
                SubjectId = 29,
                SemesterNumber = 6,
                Control = Control.Exam,
                CourseType = CourseType.Work,
                Lectures = 20,
                Labs = 3,
                Practices = 14,
                Seminars = 11,
                Independents = 31,
                Ksr = 31,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 1,
                SubjectId = 49,
                SemesterNumber = 5,
                Control = Control.Credit,
                CourseType = CourseType.Work,
                Lectures = 32,
                Labs = 30,
                Practices = 14,
                Seminars = 11,
                Independents = 35,
                Ksr = 12,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 2,
                SubjectId = 12,
                SemesterNumber = 2,
                Control = Control.Credit,
                CourseType = CourseType.Project,
                Lectures = 13,
                Labs = 15,
                Practices = 1,
                Seminars = 35,
                Independents = 11,
                Ksr = 38,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 2,
                SubjectId = 7,
                SemesterNumber = 4,
                Control = Control.Credit,
                CourseType = CourseType.Project,
                Lectures = 36,
                Labs = 20,
                Practices = 14,
                Seminars = 37,
                Independents = 15,
                Ksr = 24,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 2,
                SubjectId = 23,
                SemesterNumber = 1,
                Control = Control.Exam,
                CourseType = CourseType.Work,
                Lectures = 6,
                Labs = 24,
                Practices = 6,
                Seminars = 4,
                Independents = 35,
                Ksr = 30,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 2,
                DepartmentId = 1,
                SubjectId = 35,
                SemesterNumber = 3,
                Control = Control.Exam,
                CourseType = CourseType.Work,
                Lectures = 8,
                Labs = 6,
                Practices = 22,
                Seminars = 25,
                Independents = 20,
                Ksr = 22,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 2,
                DepartmentId = 1,
                SubjectId = 26,
                SemesterNumber = 8,
                Control = Control.Credit,
                CourseType = CourseType.Project,
                Lectures = 36,
                Labs = 8,
                Practices = 39,
                Seminars = 30,
                Independents = 40,
                Ksr = 3,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 2,
                SubjectId = 35,
                SemesterNumber = 6,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 32,
                Labs = 9,
                Practices = 21,
                Seminars = 26,
                Independents = 1,
                Ksr = 6,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 1,
                SubjectId = 33,
                SemesterNumber = 5,
                Control = Control.Exam,
                CourseType = CourseType.Work,
                Lectures = 14,
                Labs = 1,
                Practices = 30,
                Seminars = 14,
                Independents = 20,
                Ksr = 38,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 2,
                DepartmentId = 2,
                SubjectId = 12,
                SemesterNumber = 4,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 17,
                Labs = 37,
                Practices = 3,
                Seminars = 1,
                Independents = 24,
                Ksr = 37,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 2,
                DepartmentId = 1,
                SubjectId = 7,
                SemesterNumber = 3,
                Control = Control.Credit,
                CourseType = CourseType.Project,
                Lectures = 7,
                Labs = 30,
                Practices = 27,
                Seminars = 37,
                Independents = 25,
                Ksr = 35,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 1,
                SubjectId = 17,
                SemesterNumber = 5,
                Control = Control.Credit,
                CourseType = CourseType.Project,
                Lectures = 7,
                Labs = 36,
                Practices = 20,
                Seminars = 24,
                Independents = 39,
                Ksr = 4,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 2,
                SubjectId = 23,
                SemesterNumber = 7,
                Control = Control.Credit,
                CourseType = CourseType.Work,
                Lectures = 32,
                Labs = 1,
                Practices = 34,
                Seminars = 40,
                Independents = 15,
                Ksr = 27,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 2,
                SubjectId = 34,
                SemesterNumber = 6,
                Control = Control.Exam,
                CourseType = CourseType.Work,
                Lectures = 16,
                Labs = 27,
                Practices = 18,
                Seminars = 27,
                Independents = 10,
                Ksr = 21,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 2,
                DepartmentId = 2,
                SubjectId = 42,
                SemesterNumber = 3,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 29,
                Labs = 3,
                Practices = 39,
                Seminars = 24,
                Independents = 39,
                Ksr = 32,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 1,
                SubjectId = 31,
                SemesterNumber = 7,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 39,
                Labs = 1,
                Practices = 33,
                Seminars = 39,
                Independents = 18,
                Ksr = 31,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 1,
                SubjectId = 25,
                SemesterNumber = 4,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 30,
                Labs = 12,
                Practices = 31,
                Seminars = 4,
                Independents = 2,
                Ksr = 2,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 1,
                SubjectId = 27,
                SemesterNumber = 1,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 1,
                Labs = 27,
                Practices = 10,
                Seminars = 38,
                Independents = 1,
                Ksr = 1,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 1,
                SubjectId = 23,
                SemesterNumber = 6,
                Control = Control.Credit,
                CourseType = CourseType.Work,
                Lectures = 7,
                Labs = 20,
                Practices = 4,
                Seminars = 30,
                Independents = 15,
                Ksr = 21,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 1,
                SubjectId = 36,
                SemesterNumber = 6,
                Control = Control.Credit,
                CourseType = CourseType.Project,
                Lectures = 23,
                Labs = 11,
                Practices = 12,
                Seminars = 24,
                Independents = 12,
                Ksr = 6,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 2,
                SubjectId = 11,
                SemesterNumber = 6,
                Control = Control.Exam,
                CourseType = CourseType.Work,
                Lectures = 28,
                Labs = 26,
                Practices = 21,
                Seminars = 39,
                Independents = 29,
                Ksr = 8,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 1,
                SubjectId = 43,
                SemesterNumber = 1,
                Control = Control.Credit,
                CourseType = CourseType.Project,
                Lectures = 29,
                Labs = 21,
                Practices = 25,
                Seminars = 10,
                Independents = 15,
                Ksr = 34,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 2,
                DepartmentId = 1,
                SubjectId = 41,
                SemesterNumber = 2,
                Control = Control.Exam,
                CourseType = CourseType.Work,
                Lectures = 36,
                Labs = 38,
                Practices = 34,
                Seminars = 39,
                Independents = 33,
                Ksr = 9,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 1,
                SubjectId = 37,
                SemesterNumber = 4,
                Control = Control.Credit,
                CourseType = CourseType.Project,
                Lectures = 33,
                Labs = 3,
                Practices = 9,
                Seminars = 35,
                Independents = 32,
                Ksr = 40,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 2,
                DepartmentId = 1,
                SubjectId = 17,
                SemesterNumber = 2,
                Control = Control.Credit,
                CourseType = CourseType.Work,
                Lectures = 25,
                Labs = 9,
                Practices = 29,
                Seminars = 7,
                Independents = 16,
                Ksr = 13,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 2,
                SubjectId = 31,
                SemesterNumber = 2,
                Control = Control.Credit,
                CourseType = CourseType.Work,
                Lectures = 21,
                Labs = 11,
                Practices = 40,
                Seminars = 24,
                Independents = 27,
                Ksr = 8,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 2,
                SubjectId = 7,
                SemesterNumber = 3,
                Control = Control.Credit,
                CourseType = CourseType.Work,
                Lectures = 4,
                Labs = 19,
                Practices = 8,
                Seminars = 7,
                Independents = 37,
                Ksr = 18,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 1,
                SubjectId = 37,
                SemesterNumber = 6,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 3,
                Labs = 33,
                Practices = 10,
                Seminars = 17,
                Independents = 19,
                Ksr = 14,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 2,
                SubjectId = 46,
                SemesterNumber = 6,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 6,
                Labs = 30,
                Practices = 39,
                Seminars = 21,
                Independents = 22,
                Ksr = 4,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 1,
                SubjectId = 37,
                SemesterNumber = 4,
                Control = Control.Credit,
                CourseType = CourseType.Project,
                Lectures = 12,
                Labs = 6,
                Practices = 25,
                Seminars = 8,
                Independents = 4,
                Ksr = 34,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 2,
                DepartmentId = 1,
                SubjectId = 44,
                SemesterNumber = 1,
                Control = Control.Credit,
                CourseType = CourseType.Work,
                Lectures = 20,
                Labs = 40,
                Practices = 26,
                Seminars = 29,
                Independents = 31,
                Ksr = 26,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 2,
                SubjectId = 31,
                SemesterNumber = 8,
                Control = Control.Credit,
                CourseType = CourseType.Project,
                Lectures = 11,
                Labs = 15,
                Practices = 19,
                Seminars = 14,
                Independents = 20,
                Ksr = 38,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 2,
                SubjectId = 36,
                SemesterNumber = 3,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 29,
                Labs = 15,
                Practices = 27,
                Seminars = 31,
                Independents = 24,
                Ksr = 23,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 2,
                DepartmentId = 1,
                SubjectId = 9,
                SemesterNumber = 8,
                Control = Control.Exam,
                CourseType = CourseType.Work,
                Lectures = 38,
                Labs = 27,
                Practices = 5,
                Seminars = 35,
                Independents = 9,
                Ksr = 29,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 2,
                SubjectId = 34,
                SemesterNumber = 6,
                Control = Control.Exam,
                CourseType = CourseType.Work,
                Lectures = 37,
                Labs = 8,
                Practices = 1,
                Seminars = 37,
                Independents = 23,
                Ksr = 30,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 1,
                SubjectId = 44,
                SemesterNumber = 7,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 4,
                Labs = 1,
                Practices = 3,
                Seminars = 20,
                Independents = 21,
                Ksr = 8,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 2,
                DepartmentId = 2,
                SubjectId = 46,
                SemesterNumber = 7,
                Control = Control.Credit,
                CourseType = CourseType.Project,
                Lectures = 10,
                Labs = 25,
                Practices = 21,
                Seminars = 33,
                Independents = 4,
                Ksr = 40,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 2,
                DepartmentId = 1,
                SubjectId = 50,
                SemesterNumber = 2,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 29,
                Labs = 9,
                Practices = 9,
                Seminars = 31,
                Independents = 5,
                Ksr = 34,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 2,
                SubjectId = 32,
                SemesterNumber = 2,
                Control = Control.Exam,
                CourseType = CourseType.Work,
                Lectures = 26,
                Labs = 8,
                Practices = 16,
                Seminars = 3,
                Independents = 15,
                Ksr = 33,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 2,
                DepartmentId = 1,
                SubjectId = 39,
                SemesterNumber = 6,
                Control = Control.Credit,
                CourseType = CourseType.Work,
                Lectures = 36,
                Labs = 21,
                Practices = 13,
                Seminars = 20,
                Independents = 6,
                Ksr = 33,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 1,
                SubjectId = 32,
                SemesterNumber = 1,
                Control = Control.Credit,
                CourseType = CourseType.Work,
                Lectures = 32,
                Labs = 35,
                Practices = 13,
                Seminars = 24,
                Independents = 22,
                Ksr = 25,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 1,
                SubjectId = 20,
                SemesterNumber = 6,
                Control = Control.Credit,
                CourseType = CourseType.Project,
                Lectures = 31,
                Labs = 11,
                Practices = 32,
                Seminars = 31,
                Independents = 16,
                Ksr = 25,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 1,
                SubjectId = 29,
                SemesterNumber = 1,
                Control = Control.Credit,
                CourseType = CourseType.Project,
                Lectures = 15,
                Labs = 29,
                Practices = 17,
                Seminars = 7,
                Independents = 5,
                Ksr = 30,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 2,
                SubjectId = 10,
                SemesterNumber = 5,
                Control = Control.Credit,
                CourseType = CourseType.Work,
                Lectures = 17,
                Labs = 7,
                Practices = 21,
                Seminars = 17,
                Independents = 37,
                Ksr = 38,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 1,
                SubjectId = 42,
                SemesterNumber = 2,
                Control = Control.Credit,
                CourseType = CourseType.Project,
                Lectures = 1,
                Labs = 27,
                Practices = 26,
                Seminars = 40,
                Independents = 34,
                Ksr = 27,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 2,
                SubjectId = 7,
                SemesterNumber = 8,
                Control = Control.Credit,
                CourseType = CourseType.Project,
                Lectures = 26,
                Labs = 37,
                Practices = 4,
                Seminars = 15,
                Independents = 5,
                Ksr = 2,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 1,
                SubjectId = 10,
                SemesterNumber = 3,
                Control = Control.Credit,
                CourseType = CourseType.Work,
                Lectures = 8,
                Labs = 14,
                Practices = 22,
                Seminars = 5,
                Independents = 21,
                Ksr = 6,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 2,
                DepartmentId = 2,
                SubjectId = 43,
                SemesterNumber = 2,
                Control = Control.Credit,
                CourseType = CourseType.Work,
                Lectures = 37,
                Labs = 20,
                Practices = 20,
                Seminars = 32,
                Independents = 5,
                Ksr = 32,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 1,
                SubjectId = 19,
                SemesterNumber = 6,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 23,
                Labs = 6,
                Practices = 24,
                Seminars = 40,
                Independents = 38,
                Ksr = 3,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 2,
                SubjectId = 7,
                SemesterNumber = 1,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 6,
                Labs = 38,
                Practices = 24,
                Seminars = 25,
                Independents = 23,
                Ksr = 12,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 2,
                SubjectId = 20,
                SemesterNumber = 1,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 21,
                Labs = 31,
                Practices = 32,
                Seminars = 26,
                Independents = 18,
                Ksr = 23,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 2,
                SubjectId = 14,
                SemesterNumber = 8,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 25,
                Labs = 40,
                Practices = 18,
                Seminars = 24,
                Independents = 14,
                Ksr = 39,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 2,
                DepartmentId = 1,
                SubjectId = 39,
                SemesterNumber = 3,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 34,
                Labs = 26,
                Practices = 33,
                Seminars = 7,
                Independents = 13,
                Ksr = 30,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 2,
                DepartmentId = 1,
                SubjectId = 10,
                SemesterNumber = 2,
                Control = Control.Exam,
                CourseType = CourseType.Work,
                Lectures = 15,
                Labs = 8,
                Practices = 22,
                Seminars = 38,
                Independents = 39,
                Ksr = 5,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 1,
                SubjectId = 14,
                SemesterNumber = 1,
                Control = Control.Credit,
                CourseType = CourseType.Work,
                Lectures = 19,
                Labs = 18,
                Practices = 40,
                Seminars = 13,
                Independents = 37,
                Ksr = 35,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 2,
                DepartmentId = 2,
                SubjectId = 16,
                SemesterNumber = 4,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 31,
                Labs = 37,
                Practices = 31,
                Seminars = 27,
                Independents = 30,
                Ksr = 28,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 1,
                SubjectId = 31,
                SemesterNumber = 2,
                Control = Control.Credit,
                CourseType = CourseType.Work,
                Lectures = 27,
                Labs = 28,
                Practices = 18,
                Seminars = 5,
                Independents = 31,
                Ksr = 20,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 1,
                SubjectId = 11,
                SemesterNumber = 1,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 23,
                Labs = 29,
                Practices = 19,
                Seminars = 5,
                Independents = 16,
                Ksr = 28,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 2,
                SubjectId = 34,
                SemesterNumber = 6,
                Control = Control.Exam,
                CourseType = CourseType.Work,
                Lectures = 11,
                Labs = 40,
                Practices = 24,
                Seminars = 18,
                Independents = 33,
                Ksr = 22,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 1,
                SubjectId = 17,
                SemesterNumber = 8,
                Control = Control.Credit,
                CourseType = CourseType.Work,
                Lectures = 31,
                Labs = 25,
                Practices = 26,
                Seminars = 24,
                Independents = 20,
                Ksr = 8,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 1,
                SubjectId = 21,
                SemesterNumber = 5,
                Control = Control.Credit,
                CourseType = CourseType.Project,
                Lectures = 33,
                Labs = 9,
                Practices = 11,
                Seminars = 20,
                Independents = 40,
                Ksr = 21,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 1,
                SubjectId = 15,
                SemesterNumber = 2,
                Control = Control.Credit,
                CourseType = CourseType.Work,
                Lectures = 3,
                Labs = 38,
                Practices = 24,
                Seminars = 34,
                Independents = 13,
                Ksr = 22,
                IsStreaming = true
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 2,
                SubjectId = 26,
                SemesterNumber = 3,
                Control = Control.Exam,
                CourseType = CourseType.Work,
                Lectures = 13,
                Labs = 39,
                Practices = 5,
                Seminars = 13,
                Independents = 20,
                Ksr = 32,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 1,
                DepartmentId = 1,
                SubjectId = 31,
                SemesterNumber = 5,
                Control = Control.Credit,
                CourseType = CourseType.Work,
                Lectures = 2,
                Labs = 28,
                Practices = 5,
                Seminars = 34,
                Independents = 37,
                Ksr = 10,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 2,
                DepartmentId = 1,
                SubjectId = 28,
                SemesterNumber = 1,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 4,
                Labs = 40,
                Practices = 36,
                Seminars = 30,
                Independents = 28,
                Ksr = 9,
                IsStreaming = false
            }); db.Disciplines.Add(new Discipline
            {
                PlanId = 3,
                DepartmentId = 1,
                SubjectId = 12,
                SemesterNumber = 5,
                Control = Control.Exam,
                CourseType = CourseType.Project,
                Lectures = 28,
                Labs = 16,
                Practices = 35,
                Seminars = 19,
                Independents = 15,
                Ksr = 36,
                IsStreaming = false
            });


            #endregion
            db.SaveChanges();

            #region Coefficients
            db.Coefficients.Add(new Coefficient
            {
                Name = WorkType.Offset,
                Value = 0.35
            });
            db.Coefficients.Add(new Coefficient
            {
                Name = WorkType.CourseWork,
                Value = 3
            });
            db.Coefficients.Add(new Coefficient
            {
                Name = WorkType.CourseProject,
                Value = 4
            });

            //
            //рф
            //
            db.Coefficients.Add(new Coefficient
            {
                Name = WorkType.ThesisProjectRussian,
                Value = 18
            });
            db.Coefficients.Add(new Coefficient
            {
                Name = WorkType.SecMemberAmount,
                Value = 4
            });
            db.Coefficients.Add(new Coefficient
            {
                Name = WorkType.SecChairman,
                Value = 1
            });
            db.Coefficients.Add(new Coefficient
            {
                Name = WorkType.SecMember,
                Value = 0.5
            });
            db.Coefficients.Add(new Coefficient
            {
                Name = WorkType.SecNormControl,
                Value = 0.5
            });
            db.Coefficients.Add(new Coefficient
            {
                Name = WorkType.SecApproval,
                Value = 0.5
            });
            db.Coefficients.Add(new Coefficient
            {
                Name = WorkType.SecReview,
                Value = 3
            });
            //
            //рб
            //
            //db.Coefficients.Add(new Coefficient
            //{
            //    Name = "Руководство рб",
            //    Value = 18
            //});
            //db.Coefficients.Add(new Coefficient
            //{
            //    Name = "Количество членов ГЭК рб",
            //    Value = 3
            //});
            //db.Coefficients.Add(new Coefficient
            //{
            //    Name = "Председатель часов рб",
            //    Value = 1
            //});
            //db.Coefficients.Add(new Coefficient
            //{
            //    Name = "Членов ГЭК рб",
            //    Value = 0.5
            //});
            //db.Coefficients.Add(new Coefficient
            //{
            //    Name = "Норма контроля рб",
            //    Value = 0.5
            //});
            //db.Coefficients.Add(new Coefficient
            //{
            //    Name = "Утверждение рб",
            //    Value = 0.5
            //});
            //db.Coefficients.Add(new Coefficient
            //{
            //    Name = "Рецензирование рб",
            //    Value = 3
            //});
            //
            //маг
            //
            db.Coefficients.Add(new Coefficient
            {
                Name = WorkType.GuidanceOfUndergraduates,
                Value = 15
            });
            db.Coefficients.Add(new Coefficient
            {
                Name = WorkType.UndergraduateMemberAmount,
                Value = 3
            });
            db.Coefficients.Add(new Coefficient
            {
                Name = WorkType.UndergraduateChairman,
                Value = 1
            });
            db.Coefficients.Add(new Coefficient
            {
                Name = WorkType.UndergraduateMember,
                Value = 0.5
            });
            db.Coefficients.Add(new Coefficient
            {
                Name = WorkType.UndergraduateNormControl,
                Value = 0.5
            });
            db.Coefficients.Add(new Coefficient
            {
                Name = WorkType.UndergraduateApproval,
                Value = 0.5
            });
            db.Coefficients.Add(new Coefficient
            {
                Name = WorkType.UndergraduateReview,
                Value = 4
            });

            db.Coefficients.Add(new Coefficient
            {
                Name = WorkType.Exam,
                Value = 0.5
            });
            db.Coefficients.Add(new Coefficient
            {
                Name = WorkType.Consultation,
                Value = 2
            });
            //db.Coefficients.Add(new Coefficient
            //{
            //    Name = "Консультация перед экз",
            //    Value = 2 //два часа на группу
            //});
            //db.Coefficients.Add(new Coefficient
            //{
            //    Name = "Часы кср",
            //    Value = 2
            //});
            db.Coefficients.Add(new Coefficient
            {
                Name = WorkType.ReviewingTests,
                Value = 0.75
            });
            db.Coefficients.Add(new Coefficient
            {
                Name = WorkType.RatingControl,
                Value = 0.1 //% от часов лекций
            });
            db.Coefficients.Add(new Coefficient
            {
                Name = WorkType.Internship,
                Value = 0.5 // часа в неделю на человека
            });
            db.Coefficients.Add(new Coefficient
            {
                Name = WorkType.EducationalPractice,
                Value = 36 //часов на подгруппу
            });
            #endregion
            db.SaveChanges();

            #region States
            db.StateDocumentes.Add(new StateDocument
            {
                Date = new DateTime(2017, 09, 01),
                DepartmentId = 1,
                Budget = Budget.Rf,
                FormationDate = DateTime.Now
            }); ;

            //db.StateDocumentes.Add(new StateDocument
            //{
            //    Date = new DateTime(2017, 09, 01),
            //    DepartmentId = 2,
            //    Budget = Budget.Rf
            //});
            #endregion
            db.SaveChanges();

            #region Contingents
            db.Contingents.Add(new Contingent
            {
                Date = new DateTime(2017, 09, 01),
                SpecialityId = 1
            });
            db.Contingents.Add(new Contingent
            {
                Date = new DateTime(2017, 09, 01),
                SpecialityId = 2
            });
            #endregion
            db.SaveChanges();

            #region Courses
            db.Courses.Add(new Course
            {
                ContingentId = 1,
                Number = 1,
                Budget = 10,
                OffBudget = 12,
                Foreign = 2,
                BudgetGroups = 1,
                OffBudgetGroups = 0.5
            });
            db.Courses.Add(new Course
            {
                ContingentId = 1,
                Number = 2,
                Budget = 8,
                OffBudget = 11,
                Foreign = 1,
                BudgetGroups = 0.5,
                OffBudgetGroups = 0.5
            });
            db.Courses.Add(new Course
            {
                ContingentId = 1,
                Number = 3,
                Budget = 8,
                OffBudget = 8,
                Foreign = 1,
                BudgetGroups = 0.5,
                OffBudgetGroups = 1
            });
            db.Courses.Add(new Course
            {
                ContingentId = 1,
                Number = 4,
                Budget = 6,
                OffBudget = 7,
                Foreign = 0,
                BudgetGroups = 0.5,
                OffBudgetGroups = 0.5
            });

            db.Courses.Add(new Course
            {
                ContingentId = 2,
                Number = 1,
                Budget = 10,
                OffBudget = 12,
                Foreign = 2,
                BudgetGroups = 1,
                OffBudgetGroups = 0.5
            });
            db.Courses.Add(new Course
            {
                ContingentId = 2,
                Number = 2,
                Budget = 8,
                OffBudget = 11,
                Foreign = 1,
                BudgetGroups = 0.5,
                OffBudgetGroups = 0.5
            });
            db.Courses.Add(new Course
            {
                ContingentId = 2,
                Number = 3,
                Budget = 8,
                OffBudget = 8,
                Foreign = 1,
                BudgetGroups = 0.5,
                OffBudgetGroups = 1
            });
            db.Courses.Add(new Course
            {
                ContingentId = 2,
                Number = 4,
                Budget = 6,
                OffBudget = 7,
                Foreign = 0,
                BudgetGroups = 0.5,
                OffBudgetGroups = 0.5
            });
            #endregion
            db.SaveChanges();

            #region Practices
            db.Practices.Add(new Practice
            {
                PlanId = 1,
                PracticeType = PracticeType.Educational,
                Duration = 2,
                Course = 1
            });
            db.Practices.Add(new Practice
            {
                PlanId = 1,
                PracticeType = PracticeType.Educational,
                Duration = 2,
                Course = 2
            });

            db.Practices.Add(new Practice
            {
                PlanId = 1,
                PracticeType = PracticeType.Internship,
                Duration = 2,
                Course = 3
            });

            db.Practices.Add(new Practice
            {
                PlanId = 1,
                PracticeType = PracticeType.Internship,
                Duration = 2,
                Course = 4
            });

            db.Practices.Add(new Practice
            {
                PlanId = 1,
                PracticeType = PracticeType.Internship,
                Duration = 2,
                Course = 5
            });

            #endregion
            db.SaveChanges();

            #region Settings
            db.Settings.Add(new Setting
            {
                Name = "Путь для импорта штатов"
            });
            db.Settings.Add(new Setting
            {
                Name = "Путь для шаблона штатов"
            });
            db.Settings.Add(new Setting
            {
                Name = SettingEnum.ReportIndividualPlansPath.GetDisplayName()
            });
            db.Settings.Add(new Setting
            {
                Name = SettingEnum.IndividualPlansPath.GetDisplayName()
            });
            db.Settings.Add(new Setting
            {
                Name = SettingEnum.DisciplinesInformationsPath.GetDisplayName()
            });
            db.Settings.Add(new Setting
            {
                Name = SettingEnum.IndividualPlanTempPath.GetDisplayName()
            });
            db.Settings.Add(new Setting
            {
                Name = SettingEnum.DisciplinesInformationTempPath.GetDisplayName()
            });
            db.Settings.Add(new Setting
            {
                Name = SettingEnum.DistributionsRBPath.GetDisplayName()
            });
            db.Settings.Add(new Setting
            {
                Name = SettingEnum.DistributionsRFPath.GetDisplayName()
            });
            db.Settings.Add(new Setting
            {
                Name = SettingEnum.DisciplinesBySemesterRFPath.GetDisplayName()
            });
            db.Settings.Add(new Setting
            {
                Name = SettingEnum.DisciplinesBySemesterRBPath.GetDisplayName()
            });


            #endregion
            db.SaveChanges();

            //#region DistributionPlan
            //db.DistributionPlans.Add(new DistributionPlan
            //{
            //    DisciplineId = 1,
            //    Budget = Budget.Rf,
            //    FormStudy = FormStudy.FullTime,
            //    Stage = Stage.First,
            //    TimeOfYear = TimeOfYear.Autumn,
            //    Course = 1,
            //    BudgetStudents = 20,
            //    OffBudgetStudents = 5,
            //    BudgetGroups = 2,
            //    OffBudgetGroups = 1,
            //    Lectures = 100,
            //    Labs = 200,
            //    Practice = 20,
            //    Exams = 20
            //});
            //#endregion
            //db.SaveChanges();
        }
    }
}